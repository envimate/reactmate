# Mate-Tools for React/Frontend

React-Mate provides an architectural guideline and the tooling for composing React applications
out of several Scenes. Scenes represent the logical blocks consisting of React Container and
additional functionality, e.g. dispatching actions the moment a scene is accessed.

Model-Mate provides a backend abstraction for Redux based frontends. It helps defining a clean
way to communicate with a backend (the real one, a mocked version using Model-Mate or a combination
of both). 

## React-Mate

React-Mate is integrated in the top-level React component. The React-Mate builder accepts an
arbitrary number of scenes, that are then rendered in the DOM at the place of the `<Scenes>` tag,
based on the configuration of the distinct scene.

```
const reactMate = aReactMate()
    .withScene(Home)
    .withScene(Scene2)
    .withScene(Scene3)
    .build();
    
class App extends Component {
    render() {
        return (
            <Provider store={reactMate.store}>
                <div className="App">
                    <Scenes reactMate={reactMate} /> 
                </div>
            </Provider>);
    }
}
```

Scenes are a enhancement of simple components. In their most basic form, they combine a React component
with a name. But they can be enhanced with several integrations (see below), to give them additional functionality.
Some examples are, connecting a scene to the Redux store and attaching reducers, rendering
the scene only for specific Urls or performing actions, when the scene is accessed.

The most basic example without any integrations os shown here:
```
class Home extends Component {
    render() {
        return (
            <div>Welcome</div>
        )
    }
}

export default aSceneNamed('home')
    .rendering(
        Home
    )
```

###Integrations

Scenes or React-Mate itself can be extended with integrations.

#### Redux

Applications using Redux define their state in the Redux store. Scenes can access or `connect`
to this store similar to a normal Redux connected component.
The `connectedToReduxUsing` function accesses the same functions for mapping state or dispatch 
to props or merging them.

Reducer functionality can be attached directly to the scene. The `withTheInitialState` 
function accepts the state, which the scene's reducer should start with (= the state,
that is returned, if a reducer functions is called with `undefined` for its current state).
`reducingAction` defines a specific reducer step for a specific user event.

The reducer and its state are located in `rootState\[${sceneName}\]`.

```
const mapStateToProps = state => ({
    message: state.home.message,
});

const mapDispatchToProps = dispatch => ({
    onLogout: () => dispatch(logout()),
});

export default aSceneNamed('home')
    .rendering(
        Home,
        connectedToReduxUsing(mapStateToProps, mapDispatchToProps),
    )
    .withTheInitialState({
        message: 'welcome'
    })
    .reducingAction('USER_LOGGED_IN', (state, action) => {
        return {
            ...state,
            message: `Welcome ${action.payload.user.name}`
        }
    })
     .reducingAction('USER_LOGGED_OUT', (state, action) => {
         return {
             ...state,
             message: `Good bye ${action.payload.user.name}`
         }
     });

```

Instead of very long `reducingAction` cascades the `reducing` function can be used, 
which expects a map with actionType => handler:

```
const reducerMap = new Map();
reducerMap.set('USER_LOGGED_IN', (state, action) => ({...state, message: `Welcome ${action.payload.user.name}`}));
reducerMap.set('USER_LOGGED_OUT', (state, action) => ({...state, message: `Good bye ${action.payload.user.name}`}));

export default aSceneNamed('home')
    .rendering(
        Home,
    )
    .withTheInitialState({
        message: 'welcome'
    })
    
    .reducing(reducerMap);

```

In complexer Redux applications it's often the case, that several reducers are used and combined.
React-Mate allows therefore to incorporate several reducers in one scene. E.g in the following
example the home scene has two reducers, which divide the home scene's state in two sub states:
a `user` and a `message` sub state.


```
const mapStateToProps = state => ({
    message: state.home.messageState.message,
});

aSceneNamed('home')
    .rendering(
        Home,
        connectedToReduxUsing(mapStateToProps),
    )
    .withCustomReducer('user', homeUserStateReducer)
    .withCustomReducer('message', homeMessageStateReducer)
```

For the reducers, normal Redux reducer functions can be used, but also Redux's 
`combineReducers` function:

```
const homeUserStateReducer = combineReducers({
    a: someReducerA,
    b: someReducerB,
});
export default homeUserStateReducer;

```

React-Mate's Redux Integration also provides a builder for Redux reducer. The 
`withInitialState` and `reducingActionType` work the same as described above.

```
const homeMessageStateReducer = aReducer()
    .withInitialState({message:'Welcome'})
    .reducingActionType('USER_LOGGED_IN', (state, action) => {
        return {
            ...state,
            message: `Welcome ${action.payload.user.name}`
         }
     })
     .reducingActionType('USER_LOGGED_OUT', (state, action) => {
         return {
             ...state,
             message: `Godd bye ${action.payload.user.name}`
         }
     })
    .build();

export default homeMessageStateReducer;

```

Important Note: You can use either create a normal reducer using 
`reducingAction`/`reducing` in the scene itself or have a composed reducerspace using 
`withCustomReducer`. Not both, as otherwise collisions could occur, that could lead to
really nasty and hard to track bugs in your application!

Actors can be added and are different from reducers. Reducers change state, actors react to a state-change.

```
.actingUpon('USER_LOGGED_IN', (action, store) => {
    if(action.payload.requireRoleUpdate) {
        // State has already changed
    }
});
```

#### Hoc-little-Router
Scenes can be extended with conditional rendering. The `onlyForUrl` function
accepts a string as url, that also allows for parameter, e.g. `/user/:id`

```
export default aSceneNamed('home')
    .rendering(
        Home,
        onlyForUrl('/Home')
    )
```

### Redux-Little-Router
This integration is a plugin for React-Mate itself, that acts as an simplified abstraction
for the configuration of the Redux-Little-Router module: 
https://github.com/FormidableLabs/redux-little-router

The Router builder accepts an arbitrary number of routes given as parameter in the 
`withRoute` function. The url property is mandatory. The rest is optional (have a look at the
Redux-Little-Router README for more details).

`withRedirect` allow for redirects between to Urls given as string.

`withPageNotFound` defines which url should be used, if the visited url cannot be matched
against any of the other routes.

```
const reactMate = aReactMate()
    .withScene(Home)
    .withPlugin(aRouter()
        .withRoute({url:'/Home'})
        .withRoute({url:'/Page2'})
        .withRoute(aVisibleRoute('/blueprints', "Blueprints", 'appstore'))
        .withRedirect('/nope', '/home')
        .withPageNotFound('/home'))

const aVisibleRoute = (url, name, icon) => ({url: url, name: name, icon: icon, showInMenu: true});
```


Scenes gain the ability to have ActionListener attached, when a specific url is entered or exited.

```
export default aSceneNamed('home')
    .rendering(
        Home
    )
    .acting(uponLocationChangedActions()
        .toUrl('/Home')
        .using((state, action) => {
            console.log('Home entered');
        })
    )
    .acting(uponLocationChangedActions()
        .leavingFrom('/Home'')
        .using((state, action) => {
            console.log('Leaving home');
        })
    )

```

#### Redux Logger
tbd

#### Loading Bar
tbd


### Action Intercepting
There are cross-cutting concerns, that are expressed by events thrown at various points in the application,
that still need some central handling. A good example are events, that are thrown asynchronously as result
of a request to the backend, but resulted in an error caused by a authentication, that was timed out.

These events need some central handling, that often needs the intercepting of the given event, because
it is hard to determine, if the the error event could still render some invalid UI-State.

This integration provides an action intercepting Middleware, that gives the ability to block events,
or queue follow-up events. 

```
const GeneralErrorInterceptor = anActionInterceptorPlugin()
    .intercepting(allErrorsWith(AUTHENTICATION_ERROR_SCOPE.INVALID_TOKEN), (action, store, {propagate}) => {
        const invalidTokenAction = createInvalidTokenAction(action);
        propagate(invalidTokenAction);
    })
    .intercepting((action) => action.someProperty === invalid, (action, store, {block, propagate, dispatchAfterwards, executeAfterwards}) => {
        dispatchAfterwards(createFollowUpEvent());
        executeAfterwards(() => doSomething());
        block(action)
    })

```

The `intercepting` method takes a function as first parameter, that given the current action returns, whether the 
current action should be handled. If true is returned, the handler given as second argument is called.
The handler receives the action, the store instance and four possible actions:

`block` blocks the event and its further propagation

`propagate` takes the event, that is further propagated through other Middlewares and finally to the store itself.
It can take any arbitrary event: the original action, an enhanced version or a completely different action.

On of these methods has to be called. But for further chaining actions, two extra methods exist.

`dispatchAfterwards`: takes any event, that will be dispatched afterwards. This method can be called arbitrary times.

`executeAfterwards`: takes any function, that should be called after the current event was fully handled.
It can be called arbitrary times and the given function can dispatch events on it's own again (But take care of infinite loops)

It is integrated as a common plugin into react:

```
const reactMate = aReactMate()
    .withPlugin(anActionInterceptorPlugin()
                    .intercepting((action) => true, (action, store, {block, propagate, dispatchAfterwards, executeAfterwards}) => {
                        block(action);
                    }))       
    .build();
```

#### Dispatch Plugin

This integration allows for a more flexible way of dispatching events by adding a function `Dispatch`, that
can be called without access to a store.

```
export const anActionCreator = (someProperty) => {
    Dispatch({
           type: 'ACTION_TYPE',
           payload: {
                someProperty: someProperty
           }
        });
};

```

This especially allows for ActionCreators, that dispatch two distinct events:

```
export const openOverlayPageXWithData = (data) => {
    Dispatch({
           type: 'OVERLAY_PAGE_X/INIT',
           payload: {
                data: data
           }
        });
    Dispatch({
           type: 'OPEN_OVERLAY',
        });
};

```

In addition to dispatching in a synchronous way, two new functions are added for asynchronous
execution and dispatching. These functions can be called everywhere, even in Reducer or Middlewares

The `AsyncDispatch` function takes an event, that is dispatched when the current event's handling has finished.
If it is called outside of an ongoing dispatching process, it behaves as a normal dispatch and
dispatches the event immediately.

``` 
//inside some Reducer 
 .reducingAction('ACTION TYPE', (state, action) => {
        AsyncDispatch({
            type: 'FOLLOW_UP_ACTION_TYPE',
            payload:{}
        });
        return followUpState
    })

```

The `AsyncExecute` function behaves the same except it takes an function to execute after the
current action (or immediately if there's none). Within the function, other events can be dispatched.

This integrations is integrated into React-Mate as follows:


```
const reactMate = aReactMate()
    .withPlugin(DispatchPlugin)
    .build();

```


#### Model Mate
This integration allows to use Model-Mate. It is imported in React-Mate as a normal plugin.
For more information about Model-Mate see below:

```
const reactMate = aReactMate()
    .withPlugin(aModelMatePlugin()
        .with(aDataProvider))
```

## Model-Mate

Backend Integration made easy

ModelMate simplifies the interaction with the backend during frontend development. It provides 
easy to use interfaces to integrate with the async event-driven style of accessing a backend
in Redux. During development, the real interaction with the backend can be mocked locally
in the frontend via ModelMate. Switching to the real backend is a simple boolean and is made
completely transparent to the integrating code. 

Communication with the backend is separated in three parts: 
 - Starting the ModelMateRequest
 - Defining how a ModelMateRequest is mapped to a real fetchRequest and how the response is mocked locally
 - Listening to the requests and adapting the Redux state accordingly.

### Dispatch a ModelMateRequest

To start a request to the backend, create a ModelMateRequest, which is basically a normal Redux event
```
dispatch(ModelMate.request(USER_BACKEND_REQUEST_TYPES.FETCH))
```
The request method takes a string or an object with an `type` property, which both act as the events action type. Using an object,
provides a way to propagate data to the handling DataProvider (see below). But most of the time, data, that is needed for the fetch 
request is given as a payload, in the second parameter.
```
dispatch(ModelMate.request({type:USER_BACKEND_REQUEST_TYPES.FETCH}))
dispatch(ModelMate.request(USER_BACKEND_REQUEST_TYPES.CREATE, user))
```

### Define a DataProvider

This request is propagated and handled by a DataProvider. Your application should create a DataProvider for each logical 
isolated part of your Backend API. DataProvider define the way, how ModelMateRequests are mapped to real backend calls and
in case of simulating your backend locally, how these backend calls should be answered locally.

Each request a DataProvider should handle is configured by three Methods.

```
aDataProvider()
    .forRequest(USER_BACKEND_REQUEST_TYPES.FETCH)
    .mappingItToWebserviceRequestVia((modelMateRequest) => ModelMate.fetch('/users'))
    .simulatingWebserviceResponseLocally((fetchRequest) => {
        const userData = {};
        return LocalResponse.forSuccess(userData);
    })
```
The `forRequest` method defines, for which ModelMateRequest `type` the following two handlers are registered.

The `mappingItToWebserviceRequestVia` method is used to map the ModelMateRequest to an ModelMateWebserviceRequest 
(which is later mapped to a real FetchRequest. Explained later below in Configuration.WebserviceDecorator).
A ModelMateWebserviceRequest defines at least three things: 
 - The path to which the FetchRequest should be posted (path as `/users` in `backend.com/users`)
 - The http method to use. Default is `GET`
 - the payload, that is converted into a string representation in the `request.options.body` part of the fetchRequest
 
```
ModelMate.fetch('/users') // GET request without payload
ModelMate.fetch({path:'/users', method:'POST'}, newUser) //POST request with payload
```

The local mocking of a real backend response is defined in the `simulatingWebserviceResponseLocally` method.
It receives the same fetchRequest, that would normally be send to the backend with the `fetch` method.
It should return some sort of LocalResponse, which simulates a Promise, which resolves to a `Response` as obtained, if the
FetchRequest would be executed normally against a real backend.
The LocalResponse class defines several methods, which all take a payload. 

(When the methods are executed is explained later below. But maybe give a short explanation here too)

```
LocalResponse.forSuccess(payload)
LocalResponse.forUseCaseError(payload)
LocalResponse.forUnsupportedResponse(payload)
LocalResponse.forTextBodyError(payload)
LocalResponse.forConnectionError(payload)
```

### Accessing the Response

Independent of whether a real response or a LocalResponse was used, ModelMate maps it to a normal Redux event, that can be accessed
inside a reducer. The following examples use the ReactMate Reducer style, but in the end, a sample without ReactMate is given.

Assuming the payload of a successful response for the GET `/users` has a list of users in its payload, the integration works as follows.
```
const userDataReducer = aReducer()
    .withInitialState({users:[])
    .reducing(ModelMate.responsesOf(USER_BACKEND_REQUEST_TYPES.FETCH).using((state, response) => response.payload).onSuccess())
    .build();

```

Similar to the LocalResponse methods, several methods for responses exists. One for each possible result state.
```
ModelMate.responsesOf(USER_BACKEND_REQUEST_TYPES.FETCH).using((state, response) => ({})).onSuccess()
ModelMate.responsesOf(USER_BACKEND_REQUEST_TYPES.FETCH).using((state, response) => ({})).onConnectionError()
ModelMate.responsesOf(USER_BACKEND_REQUEST_TYPES.FETCH).using((state, response) => ({})).onTextBodyError()
ModelMate.responsesOf(USER_BACKEND_REQUEST_TYPES.FETCH).using((state, response) => ({})).onUseCaseError()
ModelMate.responsesOf(USER_BACKEND_REQUEST_TYPES.FETCH).using((state, response) => ({})).onUnsupportedResponse()
```

Additionally the following methods for commodity exists:
```
ModelMate.responsesOf(USER_BACKEND_REQUEST_TYPES.FETCH).using((state, response) => ({})).onStart()
```
This method is called, when the fetchRequests is executed.

```
ModelMate.responsesOf(USER_BACKEND_REQUEST_TYPES.FETCH).using((state, response) => ({})).onFinish()
```
This methods is called for the following results: success, useCaseError, unsupportedResponse, textBodyError, connectionError

```
ModelMate.responsesOf(USER_BACKEND_REQUEST_TYPES.FETCH).using((state, response) => ({})).onErrors()
```
This methods is called for the following results: useCaseError, unsupportedResponse, textBodyError, connectionError

```
ModelMate.responsesOf(USER_BACKEND_REQUEST_TYPES.FETCH).using((state, response) => ({})).onAllNonUseCaseErrors()
```
This methods is called for the following results: unsupportedResponse, textBodyError, connectionError

```
ModelMate.responsesOf(USER_BACKEND_REQUEST_TYPES.FETCH).using((state, response) => ({})).forAllEvents()
```
This methods is called for the following results: start, success, useCaseError, unsupportedResponse, textBodyError, connectionError

ModelMate can be used without ReactMate. Instead of given the object create with 'ModelMate.responsesOf' to a ReactMate 
ReducerBuilder, you can create a normal Redux Reducer with the `asReduxReducer` method.

```
const userDataReducer = ModelMate.responsesOf(USER_BACKEND_REQUEST_TYPES.FETCH)
    .using((state, response) => response.payload)
    .onSuccess())
    .asReduxReducer();
```


### Response format

The response for the respective results (start, success, useCaseError, unsupportedResponse, textBodyError, connectionError),
has different properties:

START
```
static forStart(fetchRequest, webserviceRequest, modelMateRequest) {
        return {
            type: `${modelMateRequest.type}_START`,
            isModelMateResponse: true,
            modelMateRequestActionType: modelMateRequest.type,
            modelMateRequest: modelMateRequest,
            modelMateWebserviceRequest: webserviceRequest,
            fetchRequest: fetchRequest,
        };
    }

```
SUCCESS
```
    static forSuccess(textBody, parsedResponse, response, fetchRequest, webserviceRequest, modelMateRequest) {
        return {
            type: `${modelMateRequest.type}_SUCCESS`,
            isModelMateResponse: true,
            modelMateRequestActionType: modelMateRequest.type,
            payload: parsedResponse.parse(),
            responseTextBody: textBody,
            parsedResponse: parsedResponse,
            response: response,
            modelMateRequest: modelMateRequest,
            modelMateWebserviceRequest: webserviceRequest,
            fetchRequest: fetchRequest,
        };
    }
```
USE_CASE_ERROR
```
    static forUseCaseError(textBody, parsedResponse, response, fetchRequest, webserviceRequest, modelMateRequest) {
        return {
            type: `${modelMateRequest.type}_ERROR_USE_CASE`,
            isModelMateResponse: true,
            modelMateRequestActionType: modelMateRequest.type,
            payload: parsedResponse.parse(),
            responseTextBody: textBody,
            parsedResponse: parsedResponse,
            response: response,
            modelMateRequest: modelMateRequest,
            modelMateWebserviceRequest: webserviceRequest,
            fetchRequest: fetchRequest,
        };
    }
```
UNSUPPORTED_RESPONSE
```
    static forUnsupportedResponse(textBody, parsedResponse, response, fetchRequest, webserviceRequest, modelMateRequest) {
        return {
            type: `${modelMateRequest.type}_ERROR_UNSUPPORTED_RESPONSE`,
            isModelMateResponse: true,
            modelMateRequestActionType: modelMateRequest.type,
            payload: parsedResponse.parse(),
            responseTextBody: textBody,
            parsedResponse: parsedResponse,
            response: response,
            modelMateRequest: modelMateRequest,
            modelMateWebserviceRequest: webserviceRequest,
            fetchRequest: fetchRequest,
        };
    }
```
TEXT_BODY_ERROR
```
    static forTextBodyError(error, response, fetchRequest, webserviceRequest, modelMateRequest) {
        return {
            type: `${modelMateRequest.type}_TEXT_BODY_ERROR`,
            isModelMateResponse: true,
            modelMateRequestActionType: modelMateRequest.type,
            error: error,
            response: response,
            modelMateRequest: modelMateRequest,
            modelMateWebserviceRequest: webserviceRequest,
            fetchRequest: fetchRequest,
        };
    }
```
CONNECTION_ERROR
```
    static forConnectionError(error, fetchRequest, webserviceRequest, modelMateRequest) {
        return {
            type: `${modelMateRequest.type}_CONNECTION_ERROR`,
            isModelMateResponse: true,
            modelMateRequestActionType: modelMateRequest.type,
            error: error,
            modelMateRequest: modelMateRequest,
            modelMateWebserviceRequest: webserviceRequest,
            fetchRequest: fetchRequest,
        };
    }
```

### Integrating ModelMate
The easiest way it to use the ReactMate tool to clue together your ReduxStore and respective integrations:
TODO: drauf hinweise, dass man das react-mate-model-mate-integration modul installieren muss

```
const backendLocal = true;
const ENDPOINT_URL = 'example.org'
const reactMate = aReactMate()
    .withPlugin(aModelMatePlugin()
        .with(userDataProvider)
        .isLocal(backendLocal)
        .withApiBasePath(ENDPOINT_URL))
```

Without ReactMate your responsible to include the ModelMateMiddleware yourself into your store.

```
const backendLocal = true;
const ENDPOINT_URL = 'example.org'
const modelMateMiddleware = aModelMate()
        .with(userDataProvider)
        .isLocal(backendLocal)
        .withApiBasePath(ENDPOINT_URL))
        
const store = createStore(
  yourApp,
  applyMiddleware(modelMateMiddleware)
)
```

### Switching between local and remote execution
 Switching between executing the FetchRequest local or remote is done by setting the flag
 during the ModelMate creation. 
 
 ```
 aModelMate().isLocal(true);
 ```


### Configuration

The For each ModelMateRequest the ModelMateMiddleware performs the following steps

- it receives a ModelMateRequest
- it locates the DataProvider which is responsible for the specific request 
- it maps the ModelMateRequest to a ModelMateWebserviceRequest as defined in the DataProvider's `mappingItToWebserviceRequestVia` method
- it creates an initial FetchRequest
- it iterates through all WebserviceRequestDecorators to configure the FetchRequest based on ModelMateWebserviceRequest
- it dispatches the START event and executes the FetchRequest (either locally or remotely)
- it parses the response into a ModelMateWebserviceResponse (see ResponseParser for more)
- it consumes the response using the configure ResponseConsumer and dispatches the respective event (SUCCESS, USE_CASE_ERROR, ...)

### WebserviceRequestDecorator
WebserviceRequestDecorator are used to insert the information provided by the ModelMateWebserviceRequest into the FetchRequest.
During the preparation of the FetchRequest, all WebserviceRequestDecorator are executed in order on the same instance of the
FetchRequest.

Per default two WebserviceRequestDecorator are defined:

1) The first WebserviceRequestDecorator sets the API_BATH_PATH to the FetchRequest's url. The API_BATH_PATH can be set with 
`withApiBasePath(path)` during the creation of the ModelMate instance. The resulting url is obtained as follows:
```
const apiBasePath = configuration.apiBasePath ? configuration.apiBasePath : '';
const path = webserviceRequest.path;
const url = `${apiBasePath}${path}`;
const decoratedFetchRequest = {...fetchRequest, url: url};
```

2) The second WebserviceRequestDecorator handles CORS, payload body and the necessary Headers. It performs the following:
 - It sets `fetchRequest.options.mode='cors` and `fetchRequest.options.method` to the requested HTTP-method
 - If method is GET, the 'Accept' header is set to 'application/json'
 - If method is not GET, the 'Content-Type' header is set to 'application/json' and the `fetchRequest.options.body' is filled
 with the stringified payload
 - if the ModelMateWebserviceRequest has an option `GETRequestData`, then the 'GETRequestData' header is filled with the stringified
 GETRequestData data.
 
WebserviceRequestDecorator can be configured during the creation of the ModelMate instance:

```
const position = 1; //Defaults to 0 if not specified
const decorator = (fetchRequest, webserviceRequest, modelMateRequest, configuration) => (fetchRequest);
aModelMate().withAWebserviceRequestDecorator(decorator, position);

```

All WebserviceRequestDecorator can be overridden with
```
const allDecorator = [decorator];
aModelMate().setAllWebserviceRequestDecorators(allDecorator);
```

Each decorator receives the following parameter:
 - fetchRequest: the current fetchRequest to be decorated
 - webserviceRequest: the webserviceRequest as obtained from the DataProvider's `mappingItToWebserviceRequestVia` method
 - modelMateRequest: the original ModelMateRequest, that was used to for the mapping within 'mappingItToWebserviceRequestVia'
 - configuration: a general configuration object, that is defined during the creation of the ModelMate instance
 
 The configuration can be set with:
 ```
 const configuration = {importantSharedProperty:'a'};
 aModelMate().withConfiguration(configuration);
 ```
 Note, that the `withApiBasePath` uses this configuration object. The 'withConfiguration' method overrides all preceding configuration,
 so you should call 'withApiBasePath' afterwards or set the 'apiBasePath' property yourself.

#### ResponseParser
The Middleware uses an instance of an ResponseParser to parse the textBody of the
FetchResponse into an ModelMateWebserviceResponse object, which provides the logic to distinguish
between a successful, useCaseError or unsupportedResponse.
 
The respective middleware logic could be simplified to:
```
const responsePromise = executeFetchRequest
responsePromise.then((response) => {
            const textBodyPromise = response.text();
            textBodyPromise.then((textBody) => {
                const parsedResponse = responseParser(response, textBody);
                if (parsedResponse.isSuccess()) {
                    //dispatch the SUCCESS ModelMateResponse
                } else if (parsedResponse.isUseCaseError()) {
                    //dispatch the USE_CASE_ERROR ModelMateResponse
                } else {
                    //dispatch the UNSUPPORTED_RESPONSE ModelMateResponse
                }
            }).catch((error) => {
                    //dispatch the TEXT_BODY_ERROR ModelMateResponse
            });
        }).catch((error) => {
                    //dispatch the CONNECTION_ERROR ModelMateResponse
        });
```
 
The default response parser creates ModelMateWebserviceResponse, which defines the SUCCESS
and USE_CASE_ERROR logic as follows:
 
Default responseParser
```
const DEFAULT_RESPONSE_PARSER = (response, textBody) =>  ModelMateWebserviceResponse.fromResponse(response, textBody);
 
```

Default ModelMateWebserviceResponse
 ```
export class ModelMateWebserviceResponse {
    constructor(status, responseBodyText) {
        this.status = status;
        this.responseBodyText = responseBodyText;
    }

    isSuccess() {
        return this.status === 200;
    }

    isUseCaseError() {
        return this.status === 400;
    }

    parse() { //Used for the payload property of several ModelMateResponse events
        const parsed = JSON.parse(this.responseBodyText);
        return parsed;
    }

    toString() {
        return JSON.stringify(this);
    }

    static fromResponse(response, responseBodyText) {
        return new ModelMateWebserviceResponse(response.status, responseBodyText);
    }
}
 ```
 
A custom distinction strategy for the SUCCESS and USE_CASE_ERROR can be implementing by
replacing the ResponseParser, with a function, that returns an object, that has the respective
methods `isSuccess`, `isUseCaseError`, `parse`. Especially implementing the parse method would
provide a way to support a different format other than JSON.

The custom responseParser can be configured with:

```
aModelMate().setResponseParser(responseParser);
```
 

 
#### ResponseConsumer

ResponseConsumer are responsible to map the result of the response to a normal Redux Event. Per default for each ConsumerState 
a ResponseConsumer is defined, that creates the respective ModelMateResponse (e.g. SUCCESS creates a `ModelMateResponse.forSuccess` as defined above)
The following ConsumerStates are defined and receive the following parameter when called:

START

parameters: decoratedWebserviceRequest, webserviceRequest, modelMateRequest


SUCCESS

parameters: textBody, parsedResponse, response, fetchRequest, webserviceRequest, modelMateRequest


USE_CASE_ERROR

parameters: textBody, parsedResponse, response, fetchRequest, webserviceRequest, modelMateRequest


UNSUPPORTED_RESPONSE

parameters: textBody, parsedResponse, response, fetchRequest, webserviceRequest, modelMateRequest


TEXT_BODY_ERROR

parameters: error, response, fetchRequest, webserviceRequest, modelMateRequest


CONNECTION_ERROR

parameters: error, fetchRequest, webserviceRequest, modelMateRequest


A ResponseConsumer can be set during creation of the ModelMateInstance:
```
const successConsumer = textBody, parsedResponse, response, fetchRequest, webserviceRequest, modelMateRequest) => {
    return ModelMateResponse.forSuccess(textBody, parsedResponse, response, fetchRequest, webserviceRequest, modelMateRequest)
}
aModelMate().withAResponseConsumer('SUCCESS', successConsumer) {
```

Alternatively all ResponseConsumer can be overridden with 
```
const responseConsumerMap = new Map();
responseConsumerMap.set('SUCCESS', successConsumer);

aModelMate().setAllResponseConsumers(responseConsumerMap) {
```


 