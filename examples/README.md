# Example for using ReactMate and ModelMate

To see the example in the browser first install the dependencies with
```
npm install
```

Afterwards you can start the sample app with

```
npm start
```