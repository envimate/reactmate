import {aDataProvider, LocalResponse, ModelMate} from "model-mate";
import {todos, updateTodos} from "../TodoData";
import TODOS_DETAILS_BACKEND_REQUESTS from "./TodosDetailsBackendRequests";


const todosDetailsDataProvider = aDataProvider()
    .forRequest(TODOS_DETAILS_BACKEND_REQUESTS.FETCH)
    .mappingItToWebserviceRequestVia((modelMateRequest) => ModelMate.fetch(`/details/${modelMateRequest.payload.id}`))
    .simulatingWebserviceResponseLocally((fetchRequest) => {
        const url = fetchRequest.url;
        const id = url.substring(url.lastIndexOf("/") + 1);
        const todosWithId = todos.filter(todo => todo.id === id);
        if (todosWithId.length === 1) {
            const todo = todosWithId[0];
            return LocalResponse.forSuccess(todo);
        } else {
            const errors = [`No todo with id ${id}`];
            return LocalResponse.forUseCaseError({errors: errors});
        }
    })
    .forRequest(TODOS_DETAILS_BACKEND_REQUESTS.UPDATE)
    .mappingItToWebserviceRequestVia((modelMateRequest) => ModelMate.fetch({path: '/details', method: 'PUT'}, modelMateRequest.payload))
    .simulatingWebserviceResponseLocally((fetchRequest) => {
        const todo = JSON.parse(fetchRequest.options.body);
        const updatedTodos = todos.map(t => t.id === todo.id ? todo : t);
        updateTodos([...updatedTodos]);
        return LocalResponse.forSuccess(todo);
    })
    .build();


export default todosDetailsDataProvider;