import {aDataProvider, LocalResponse, ModelMate} from "model-mate";
import TODOS_BACKEND_REQUESTS from "./TodosBackendRequests";
import {todos, updateTodos} from "../TodoData";


const todosDataProvider = aDataProvider()
    .forRequest(TODOS_BACKEND_REQUESTS.FETCH)
    .mappingItToWebserviceRequestVia(() => ModelMate.fetch('/todos'))
    .simulatingWebserviceResponseLocally(() => {
        return LocalResponse.forSuccess(todos);
    })
    .forRequest(TODOS_BACKEND_REQUESTS.ADD)
    .mappingItToWebserviceRequestVia((modelMateRequest) => ModelMate.fetch({path: '/todos', method: 'POST'}, modelMateRequest.payload))
    .simulatingWebserviceResponseLocally((fetchRequest) => {
        const todo = JSON.parse(fetchRequest.options.body);
        const updatedTodos = [...todos, todo];
        updateTodos(updatedTodos);
        return LocalResponse.forSuccess(updatedTodos);
    })
    .forRequest(TODOS_BACKEND_REQUESTS.DELETE)
    .mappingItToWebserviceRequestVia((modelMateRequest) => ModelMate.fetch({path: '/todos', method: 'DELETE'}, modelMateRequest.payload))
    .simulatingWebserviceResponseLocally((fetchRequest) => {
        const todoToBeDeleted = JSON.parse(fetchRequest.options.body);
        const filteredTodos = todos.filter(todo => todo.id !== todoToBeDeleted.id);
        const copiedTodos = [...filteredTodos]; //make copy, because Redux compares state shallow
        updateTodos(copiedTodos);
        return LocalResponse.forSuccess(copiedTodos);
    })
    .build();


export default todosDataProvider;