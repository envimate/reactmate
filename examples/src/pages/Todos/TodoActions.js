import {ModelMate} from "model-mate";
import {push} from 'redux-little-router';
import TODOS_BACKEND_REQUESTS from "../../backend/todos/TodosBackendRequests";

export const TODO_ACTION_TYPES = {
    SET_NEW_TODO_TITLE: "TODOS/SET_NEW_TODO_TITLE",
};

export function setNewTodosTitle(newTitle) {
    return {
        type: TODO_ACTION_TYPES.SET_NEW_TODO_TITLE,
        payload: {
            newTitle: newTitle
        }
    }
}

export function addTodo(todo) {
    return ModelMate.request(TODOS_BACKEND_REQUESTS.ADD, todo)
}

export function deleteTodo(todo) {
    return ModelMate.request(TODOS_BACKEND_REQUESTS.DELETE, todo)
}

export function showAllTodos() {
    return push('/todos');
}