import TODOS_BACKEND_REQUESTS from "../../backend/todos/TodosBackendRequests";
import {aReduxReducer} from "react-mate-react-redux-integration";
import {TODO_ACTION_TYPES} from "./TodoActions";

const initialState = {
    newTodoTitle: ''
};

const todosUiStateReducer = aReduxReducer()
    .withInitialState(initialState)
    .reducingActionType(TODO_ACTION_TYPES.SET_NEW_TODO_TITLE, (state, action) => ({...state, newTodoTitle: action.payload.newTitle}))
    .reducingActionType(TODOS_BACKEND_REQUESTS.ADD, (state, action) => ({...state, newTodoTitle: ''}))
    .build();

export default todosUiStateReducer;