import React from "react";
import {onlyForUrl} from "react-mate-hoc-little-router-integration";
import {aSceneNamed} from "react-mate";
import {uponLocationChangedActions} from "react-mate-redux-little-router-integration";
import todosDataReducer from "./TodosDataReducer";
import {ModelMate} from "model-mate";
import TODOS_BACKEND_REQUESTS from "../../backend/todos/TodosBackendRequests";
import {Header, PageHeader} from "../../components/header/PageHeader";
import TodoList from "../../components/todo-list/TodoList";
import {connectedToReduxUsing} from "react-mate-react-redux-integration";
import {Button, Col, Input, Row} from "antd";
import todosUiStateReducer from "./TodosUiStateReducer";
import {addTodo, deleteTodo, setNewTodosTitle} from "./TodoActions";
import generateANotSecureOrSufficientlyUniqueUuid from "../../shared/uuid/UUID"
import {showTodoDetails} from "../todo-details/TodoDetailsActions";
import {PageContent} from "../../components/page-content/PageContent";

class Todos extends React.Component {

    onChangeNewTodoTitle = (event) => {
        this.props.onChangeNewTodoTitle(event.target.value)
    };

    addTodo = () => {
        this.props.onAddTodo({
            id: generateANotSecureOrSufficientlyUniqueUuid(),
            title: this.props.newTodoTitle,
            description: ''
        })
    };

    render() {
        return (
            <div>
                <PageHeader title={'Todos'}/>
                <PageContent>
                    <TodoList todos={this.props.todos} onDelete={this.props.onDelete} onDetails={this.props.onDetails}/>
                    <div style={{paddingTop: '30px'}}>
                        <Row>
                            <Col span='4'>
                            </Col>
                            <Col span='8'>
                                <Input value={this.props.newTodoTitle} onChange={this.onChangeNewTodoTitle} placeholder='title'/>
                            </Col>
                            <Col span='2'>
                            </Col>
                            <Col span='4'>
                                <Button type='primary' onClick={this.addTodo}>Add</Button>
                            </Col>
                        </Row>
                    </div>
                </PageContent>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    const todoState = state.todoScene;
    return {
        todos: todoState.todos,
        newTodoTitle: todoState.ui.newTodoTitle,
    }
};

const mapDispatchToProps = (dispatch) => ({
    onChangeNewTodoTitle: (newTitle) => dispatch(setNewTodosTitle(newTitle)),
    onAddTodo: (todo) => dispatch(addTodo(todo)),
    onDelete: (todo) => dispatch(deleteTodo(todo)),
    onDetails: (todo) => dispatch(showTodoDetails(todo)),
});

export default aSceneNamed('todoScene')
    .rendering(
        Todos,
        onlyForUrl('/todos'),
        connectedToReduxUsing(mapStateToProps, mapDispatchToProps)
    )
    .acting(uponLocationChangedActions().toUrl("/todos").using((action, store) => {
        store.dispatch(ModelMate.request(TODOS_BACKEND_REQUESTS.FETCH))
    }))
    .withCustomReducer('todos', todosDataReducer)
    .withCustomReducer('ui', todosUiStateReducer)

