import {ModelMate} from "model-mate";
import TODOS_BACKEND_REQUESTS from "../../backend/todos/TodosBackendRequests";
import {aReduxReducer} from "react-mate-react-redux-integration";

const initialState = [];

const todosDataReducer = aReduxReducer()
    .withInitialState(initialState)
    .reducing(ModelMate.responsesOf(TODOS_BACKEND_REQUESTS.FETCH).using((state, response) => response.payload).onSuccess())
    .reducing(ModelMate.responsesOf(TODOS_BACKEND_REQUESTS.ADD).using((state, response) => response.payload).onSuccess())
    .reducing(ModelMate.responsesOf(TODOS_BACKEND_REQUESTS.DELETE).using((state, response) => response.payload).onSuccess())
    .build();

export default todosDataReducer;