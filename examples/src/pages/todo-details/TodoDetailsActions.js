import {push} from 'redux-little-router';
import {ModelMate} from "model-mate";
import TODOS_DETAILS_BACKEND_REQUESTS from "../../backend/details/TodosDetailsBackendRequests";

export function showTodoDetails(todo) {
    return push(`/details/${todo.id}`);
}

export function updateTodo(todo) {
    return ModelMate.request(TODOS_DETAILS_BACKEND_REQUESTS.UPDATE, todo)
}