import React from "react";
import {onlyForUrl} from "react-mate-hoc-little-router-integration";
import {aSceneNamed} from "react-mate";
import {uponLocationChangedActions} from "react-mate-redux-little-router-integration";
import {ModelMate} from "model-mate";
import {Header, PageHeader} from "../../components/header/PageHeader";
import {connectedToReduxUsing} from "react-mate-react-redux-integration";
import {Button, Col, Icon, Input, Row} from "antd";
import todosDetailsDataReducer from "./TodosDetailsDataReducer";
import TODOS_DETAILS_BACKEND_REQUESTS from "../../backend/details/TodosDetailsBackendRequests";
import {updateTodo} from "./TodoDetailsActions";
import {showAllTodos} from "../Todos/TodoActions";
import todosWebserviceStateReducer from "./TodosWebserviceStateReducer";
import {PageContent} from "../../components/page-content/PageContent";

const {TextArea} = Input;

class Todos extends React.Component {

    onChangeDescription = (event) => {
        const updatedTodo = {...this.props.todo, description: event.target.value};
        this.props.updateTodo(updatedTodo);
    };

    componentDidUpdate = () => {
        if (this.props.redirectToOverview) {
            this.props.onRedirectToOverview();
        }
    };

    render() {
        const todo = this.props.todo;
        return (
            <div>
                <PageHeader title={'Todo Details'}/>
                <PageContent>
                    <Row>
                        <Col span='5'>
                            Title:
                        </Col>
                        <Col span='8'>
                            {todo.title}
                        </Col>
                    </Row>

                    <Row>
                        <Col span='5'>
                            Description:
                        </Col>
                        <Col span='8'>
                            <TextArea rows='4' value={todo.description} onChange={this.onChangeDescription}/>
                        </Col>
                    </Row>
                    <div style={{paddingTop: '30px'}}>
                        <Button onClick={this.props.onGoBack} type='primary'><Icon type="left"/>goBack</Button>
                    </div>
                </PageContent>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    const todoDetailsState = state.todoDetailsScene;
    return {
        todo: todoDetailsState.todo,
        redirectToOverview: todoDetailsState.webservice.redirectToOverview,
    }
};

const mapDispatchToProps = (dispatch) => ({
    updateTodo: (todo) => dispatch(updateTodo(todo)),
    onGoBack: () => dispatch(showAllTodos()),
    onRedirectToOverview: () => dispatch(showAllTodos()),
});

export default aSceneNamed('todoDetailsScene')
    .rendering(
        Todos,
        onlyForUrl('/details/:id'),
        connectedToReduxUsing(mapStateToProps, mapDispatchToProps)
    )
    .acting(uponLocationChangedActions().toUrl("/details/:id").using((action, store) => {
        store.dispatch(ModelMate.request(TODOS_DETAILS_BACKEND_REQUESTS.FETCH, {
            id: store.getState().router.params.id, //the param id is stored in the state of the Redux-little-router
        }))
    }))
    .withCustomReducer('todo', todosDetailsDataReducer)
    .withCustomReducer('webservice', todosWebserviceStateReducer)