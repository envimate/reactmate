import {ModelMate} from "model-mate";
import {aReduxReducer} from "react-mate-react-redux-integration";
import TODOS_DETAILS_BACKEND_REQUESTS from "../../backend/details/TodosDetailsBackendRequests";

const initialState = {};

const todosDetailsDataReducer = aReduxReducer()
    .withInitialState(initialState)
    .reducing(ModelMate.responsesOf(TODOS_DETAILS_BACKEND_REQUESTS.FETCH).using((state, response) => response.payload).onSuccess())
    .reducing(ModelMate.responsesOf(TODOS_DETAILS_BACKEND_REQUESTS.UPDATE).using((state, response) => response.payload).onSuccess())
    .build();

export default todosDetailsDataReducer;