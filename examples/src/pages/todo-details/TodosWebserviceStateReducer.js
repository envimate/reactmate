import {ModelMate} from "model-mate";
import {aReduxReducer} from "react-mate-react-redux-integration";
import TODOS_DETAILS_BACKEND_REQUESTS from "../../backend/details/TodosDetailsBackendRequests";

const initialState = {
    redirectToOverview: false
};

const todosWebserviceStateReducer = aReduxReducer()
    .withInitialState(initialState)
    .reducing(ModelMate.responsesOf(TODOS_DETAILS_BACKEND_REQUESTS.FETCH).using((state, response) => ({redirectToOverview:false})).onStart())
    .reducing(ModelMate.responsesOf(TODOS_DETAILS_BACKEND_REQUESTS.FETCH).using((state, response) => ({redirectToOverview:false})).onSuccess())
    .reducing(ModelMate.responsesOf(TODOS_DETAILS_BACKEND_REQUESTS.FETCH).using((state, response) => ({redirectToOverview:true})).onUseCaseError())
    .build();

export default todosWebserviceStateReducer;