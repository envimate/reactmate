import React, {Component} from 'react';
import './App.css';
import {Provider} from "react-redux";
import {aReactMate, Scenes} from "react-mate";
import Todos from "./pages/Todos/Todos";
import {aRouter} from "react-mate-redux-little-router-integration";
import Layout from "antd/es/layout/layout";
import SideMenu from "./components/side-menu/SideMenu";
import {aModelMatePlugin} from "react-mate-model-mate-integration";
import todosDataProvider from "./backend/todos/TodoDataProvider";
import TodoDetails from "./pages/todo-details/TodoDetails";
import todosDetailsDataProvider from "./backend/details/TodoDetailsDataProvider";


const aVisibleRoute = (url, name, icon) => ({url: url, name: name, icon: icon, showInMenu: true});
const aHiddenRoute = (url, name) => ({url: url, name: name, showInMenu: false});

const ENDPOINT_URL = process.env.REACT_APP_END_POINT_URL || 'http://example.com';

const reactMate = aReactMate()
    .withScene(Todos)
    .withScene(TodoDetails)
    .withPlugin(aRouter()
        .withRoute(aVisibleRoute('/todos', "Todos", 'home'))
        .withRoute(aHiddenRoute('/details/:id', "Details"))
        .withPageNotFound('/todos'))
    .withPlugin(aModelMatePlugin()
        .with(todosDataProvider)
        .with(todosDetailsDataProvider)
        .isLocal(true)
        .withApiBasePath(ENDPOINT_URL))
    .build();

class App extends Component {
    render() {
        return (
            <Provider store={reactMate.store}>
                <Layout>
                    <SideMenu/>
                    <Layout>
                        <Scenes reactMate={reactMate}/>
                    </Layout>
                </Layout>
            </Provider>
        );
    }
}

export default App;
