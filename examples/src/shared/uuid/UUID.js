export default function generateANotSecureUuid() {
    return "" + parseInt(Math.random() * 99 + 1, 10);
}
