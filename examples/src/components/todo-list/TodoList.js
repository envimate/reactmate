import React from "react";
import {Button, List} from "antd";
import PropTypes from "prop-types";

export default class TodoList extends React.Component {

    onDeleteHandler = (todo) => () => {
        this.props.onDelete(todo);
    };

    onDetailsHandler = (todo) => () => {
        this.props.onDetails(todo);
    };

    render() {
        const todos = this.props.todos;
        return (
            <List
                itemLayout="horizontal"
                dataSource={todos}
                renderItem={todo => (
                    <List.Item>
                        <List.Item.Meta
                            title={todo.title}
                            description={todo.description}
                        />
                        <Button type='default' onClick={this.onDetailsHandler(todo)}>Details</Button>
                        <Button type='danger' onClick={this.onDeleteHandler(todo)}>Delete</Button>
                    </List.Item>
                )}
            />
        )
    }
}

TodoList.propTypes = {
    todos: PropTypes.arrayOf(PropTypes.object).isRequired,
    onDelete: PropTypes.func.isRequired,
    onDetails: PropTypes.func.isRequired,
};