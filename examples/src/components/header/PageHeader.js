import React from 'react';
import {Layout} from "antd";
import PropTypes from "prop-types";

const {Header} = Layout;

export class PageHeader extends React.Component {

    render() {
        return (
            <Header style={{backgroundColor: 'white', textAlign: 'left'}}>
                    <span style={{color: 'rgb(1, 3, 5', fontSize: '2em', fontWeight: '700'}}>
                    {this.props.title}
                </span>
            </Header>
        );
    }
}

PageHeader.propTypes = {
    title: PropTypes.node.isRequired,
};


