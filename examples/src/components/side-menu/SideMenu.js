import React from 'react';
import {Icon, Layout, Menu} from "antd";
import {Link} from "redux-little-router";
import {connect} from "react-redux";

const {Sider} = Layout;

class SideMenu extends React.Component {
    constructor() {
        super();
        this.state = {collapsed: true};

        this.startHover = this.startHover.bind(this);
        this.endHover = this.endHover.bind(this);
    }

    startHover() {
        this.setState(prevState => ({
            collapsed: false
        }))
    }

    endHover() {
        this.setState(prevState => ({
            collapsed: true
        }))
    }

    render() {
        const routes = this.props.routes;
        const visibleMenuItems = Object.getOwnPropertyNames(routes)
            .filter(propertyName => {
                return routes[propertyName].showInMenu
            });
        const menu = visibleMenuItems.map(propertyName => {
            const route = routes[propertyName];
            return (
                <Menu.Item key={propertyName} style={{'textAlign': 'left'}}>
                    <Icon type={route.icon}/>
                    <span>
                        <Link href={route.url}>
                            <span style={{'color': 'rgba(0, 0, 0, 0.65)'}}>{route.name}</span>
                        </Link>
                    </span>
                </Menu.Item>)
        });

        const defaultSelectedKeys = visibleMenuItems[0];
        const activeUrl = this.props.activeUrl;
        return (
            <Sider
                trigger={null}
                collapsible
                collapsed={this.state.collapsed}
                onMouseEnter={this.startHover}
                onMouseLeave={this.endHover}
                style={{'backgroundColor': '#fff'}}>
                <div className="logo"/>
                <Menu theme="light" mode="inline"
                      selectedKeys={[activeUrl]}
                      defaultSelectedKeys={[defaultSelectedKeys]}
                      style={{height: '100%'}}>
                    {menu}
                </Menu>
            </Sider>)
    }
}

const mapStateToProps = state => {
    let activeUrl;
    if (state.router.result) {
        activeUrl = state.router.result.parent ? state.router.result.parent.route : state.router.pathname
    } else {
        activeUrl = state.router.pathname;
    }
    return {
        routes: state.router.routes,
        activeUrl: activeUrl,
    }
};

export default connect(mapStateToProps)(SideMenu)