import babel from 'rollup-plugin-babel'
import uglify from 'rollup-plugin-uglify'

const env = process.env.NODE_ENV;
const config = {
    input: 'src/index.js',
    plugins: [
        babel({
        exclude: 'node_modules/**',
        plugins: ['external-helpers'],
    })],
};

if (env === 'es') {
    config.output = {format: 'es', name: 'model-mate', indent: false, file:'es/model-mate.js'};
}

if (env === 'cjs' ) {
    config.output = {format: 'cjs', name: 'model-mate', indent: false, file: 'lib/model-mate.js'};
}

if (env === 'umd') {
    config.output = {format: 'umd', name: 'model-mate', indent: false, file: 'dist/model-mate.umd.js'}
}

if (env === 'umd-min') {
    config.output = {format: 'umd', name: 'model-mate', indent: false, file: 'dist/model-mate.umd.min.js'};
    config.plugins.push(
        uglify({
            compress: {
                pure_getters: true,
                unsafe: true,
                unsafe_comps: true,
                warnings: false
            }
        })
    )
}

export default config