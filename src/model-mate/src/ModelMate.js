import {ModelMateRequest} from "./request/ModelMateRequest";
import {aModelMateMiddleware} from "./middleware/ModelMateMiddleware";
import ModelMateWebserviceRequest from "./request/ModelMateWebserviceRequest";
import {ResponseHandlerBuilder} from "./response/ResponseHandlerBuilder";
import {getDefaultWebserviceRequestDecorators} from "./request/WebserviceRequestDecorator";
import {getDefaultResponseParser} from "./response/ResponseParser";
import {getDefaultResponseConsumers} from "./response/ResponseConsumer";

export class ModelMate {

    static request(modelMateRequest, payload) {
        const modelMateRequestObject = ModelMateRequest.createNew(modelMateRequest, payload);
        return modelMateRequestObject;
    }

    static fetch(webServiceRequest, payload) {
        const modelMateWebServiceRequestObject = ModelMateWebserviceRequest.createNew(webServiceRequest, payload);
        return modelMateWebServiceRequestObject;
    }

    static responsesOf(modelMateRequest) {
        const modelMateRequestObject = ModelMateRequest.createNew(modelMateRequest);
        return new ResponseHandlerBuilder(modelMateRequestObject.type);
    }
}

export class ModelMateBuilder {

    constructor() {
        this._middlewareBuilder = aModelMateMiddleware();
        this._middlewareBuilder.setResponseParser(getDefaultResponseParser());
        this._middlewareBuilder.setAllWebserviceRequestDecorators(getDefaultWebserviceRequestDecorators());
        this._middlewareBuilder.setAllResponseConsumers(getDefaultResponseConsumers());
        this._isLocal = false;
        this._configuration = {};
    }

    with(dataProvider) {
        this._middlewareBuilder.with(dataProvider);
        return this;
    }

    isLocal(isLocal) {
        this._isLocal = isLocal;
        return this;
    }

    withApiBasePath(apiBasePath) {
        this._configuration = {...this._configuration, apiBasePath: apiBasePath};
        return this;
    }

    withConfiguration(configuration) {
        this._configuration = configuration;
        return this;
    }

    withAWebserviceRequestDecorator(webserviceRequestDecorator, position) {
        this._middlewareBuilder.withAWebserviceRequestDecorator(webserviceRequestDecorator, position);
        return this;
    }

    setAllWebserviceRequestDecorator(webserviceRequestDecorators) {
        this._middlewareBuilder.setAllWebserviceRequestDecorators(webserviceRequestDecorators);
        return this;
    }

    setResponseParser(responseParser) {
        this._middlewareBuilder.setResponseParser(responseParser);
        return this;
    }

    withAResponseConsumer(responseState, responseConsumer) {
        this._middlewareBuilder.withAResponseConsumer(responseState, responseConsumer);
        return this;
    }

    setAllResponseConsumers(responseConsumerMap) {
        this._middlewareBuilder.setAllResponseConsumers(responseConsumerMap);
        return this;
    }

    withADataProviderMiddleware(dataProviderMiddleware){
        this._middlewareBuilder.withADataProviderMiddleware(dataProviderMiddleware);
        return this;
    }

    build() {
        this._middlewareBuilder.isLocal(this._isLocal);
        this._middlewareBuilder.withConfiguration(this._configuration);
        const middleware = this._middlewareBuilder.build();
        return middleware;
    }
}


export const aModelMate = () => {
    return new ModelMateBuilder();
};
