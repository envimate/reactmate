export function aModelMateMiddleware() {
    const webserviceRequestDecorators = [];
    const responseParser = null;
    const responseConsumers = new Map();
    return new ModelMateMiddlewareBuilder(webserviceRequestDecorators, responseParser, responseConsumers);
}


class ModelMateMiddlewareBuilder {

    constructor(webserviceRequestDecorators, responseParser, responseConsumers) {
        this._dataProviders = new Map();
        this._responseParser = responseParser;
        this._isLocal = true;
        this._webserviceRequestDecorators = webserviceRequestDecorators;
        this._responseConsumers = responseConsumers;
        this._dataProviderMiddlewares = [];
        this._configuration = {};
    }

    with(dataProviderMap) {
        dataProviderMap.forEach((provider, type) => {
            this._addDataProvider(type, provider)
        });
        return this;
    }

    _addDataProvider(type, dataProvider) {
        if (this._dataProviders.has(type)) {
            throw new Error(`Tried to add DataProvider for type ${type}, but there was already one registered.`);
        } else {
            this._dataProviders.set(type, dataProvider);
        }
    }

    withAWebserviceRequestDecorator(webserviceRequestDecorator, position) {
        if (!!position) {
            this._webserviceRequestDecorators.splice(position, 0, webserviceRequestDecorator);
        } else {
            this._webserviceRequestDecorators.push(webserviceRequestDecorator);
        }
        return this;
    }

    setAllWebserviceRequestDecorators(webserviceRequestDecorators) {
        this._webserviceRequestDecorators = webserviceRequestDecorators;
        return this;
    }


    setResponseParser(responseParser) {
        this._responseParser = responseParser;
        return this;
    }

    withAResponseConsumer(responseState, responseConsumer) {
        this._responseConsumers.set(responseState, responseConsumer);
        return this;
    }

    setAllResponseConsumers(responseConsumerMap) {
        this._responseConsumers = responseConsumerMap;
        return this;
    }


    isLocal(isLocal) {
        this._isLocal = isLocal;
        return this;
    }

    withConfiguration(configuration) {
        this._configuration = configuration;
        return this;
    }

    withADataProviderMiddleware(dataProviderMiddleware) {
        this._dataProviderMiddlewares.push(dataProviderMiddleware);
        return this;
    }

    build() {
        if (!this._responseParser) {
            throw new Error(`ModelMateMiddleware needs a responseParser object being set with ${this.setResponseParser.name}`);
        }
        return ModelMateMiddleware.aModelMateMiddleware(this._dataProviders, this._webserviceRequestDecorators, this._dataProviderMiddlewares,
            this._responseParser, this._responseConsumers, this._isLocal, this._configuration);
    }
}

class ModelMateMiddleware {
    static aModelMateMiddleware(dataProviders, decorators, dataProviderMiddlewares, responseParser, responseConsumers, isLocal, configuration) {
        return new ModelMateMiddleware(dataProviders, decorators, dataProviderMiddlewares, responseParser, responseConsumers, isLocal, configuration);
    }

    constructor(dataProviders, decorators, dataProviderMiddlewares, responseParser, responseConsumers, isLocal, configuration) {
        this._dataProviders = dataProviders;
        this._webserviceRequestDecorators = decorators;
        this._responseParser = responseParser;
        this._responseConsumers = responseConsumers;
        this._isLocal = isLocal;
        this._configuration = configuration;
        return (store) => (next) => (action) => {
            next(action);
            if (action.isModelMateRequest) {
                const modelMateRequest = action;
                const type = modelMateRequest.type;
                const dataProvider = this._getDataProviderFor(type);
                const webserviceRequest = dataProvider.mapToWebserviceRequest(modelMateRequest);

                const initialFetchRequest = {url: webserviceRequest.path, options: {}};
                const fetchRequest = this._decorate(initialFetchRequest, webserviceRequest, modelMateRequest, this._configuration);
                const webserviceStartEvent = this._startEventFor(fetchRequest, webserviceRequest, modelMateRequest);
                store.dispatch(webserviceStartEvent);

                const responsePromise = this._handleFetchRequest(fetchRequest, dataProvider, dataProviderMiddlewares);
                this._consume(store.dispatch, responsePromise, fetchRequest, webserviceRequest, modelMateRequest);
            }
        }
    }

    _getDataProviderFor(type) {
        if (this._dataProviders.has(type)) {
            return this._dataProviders.get(type);
        } else {
            throw new Error(`Could not find a dataProvider for a ModelMateRequest of type ${type}`);
        }
    }

    _decorate(initialFetchRequest, webserviceRequest, modelMateRequest, configuration) {
        let fetchRequest = initialFetchRequest;
        for (let decorator of this._webserviceRequestDecorators) {
            fetchRequest = decorator(fetchRequest, webserviceRequest, modelMateRequest, configuration);
        }
        return fetchRequest;
    }

    _startEventFor(decoratedWebserviceRequest, webserviceRequest, modelMateRequest) {
        const consumer = this._responseConsumerFor('START');
        return consumer(decoratedWebserviceRequest, webserviceRequest, modelMateRequest)
    }

    _responseConsumerFor(responseState) {
        if (this._responseConsumers.has(responseState)) {
            return this._responseConsumers.get(responseState)
        } else {
            return () => null;
        }
    }

    _handleFetchRequest(fetchRequest, dataProvider, dataProviderMiddlewares) {
        if (this._isLocal) {
            const dataProviderConfiguration = dataProvider.getConfiguration();
            let nextDataProviderMiddlewareIndex = 0;
            const next = (fetchRequest) => {
                if (nextDataProviderMiddlewareIndex < dataProviderMiddlewares.length) {
                    return dataProviderMiddlewares[nextDataProviderMiddlewareIndex++](next, fetchRequest, dataProviderConfiguration);
                } else {
                    const handler = dataProvider.getLocalWebserviceHandler();
                    return handler(fetchRequest);
                }
            };
            return next(fetchRequest);

        } else {
            const handler = dataProvider.getRemoteWebserviceHandler();
            return handler(fetchRequest);
        }
    }

    _consume(storeDispatch, responsePromise, fetchRequest, webserviceRequest, modelMateRequest) {
        const dispatch = (event) => event ? storeDispatch(event) : undefined;
        responsePromise.then((response) => {
            const textBodyPromise = response.text();
            textBodyPromise.then((textBody) => {
                const parsedResponse = this._responseParser(response, textBody);
                if (parsedResponse.isSuccess()) {
                    const consumer = this._responseConsumerFor('SUCCESS');
                    const modelMateResponse = consumer(textBody, parsedResponse, response, fetchRequest, webserviceRequest, modelMateRequest);
                    dispatch(modelMateResponse);
                } else if (parsedResponse.isUseCaseError()) {
                    const consumer = this._responseConsumerFor('USE_CASE_ERROR');
                    const modelMateResponse = consumer(textBody, parsedResponse, response, fetchRequest, webserviceRequest, modelMateRequest);
                    dispatch(modelMateResponse);
                } else {
                    const consumer = this._responseConsumerFor('UNSUPPORTED_RESPONSE');
                    const modelMateResponse = consumer(textBody, parsedResponse, response, fetchRequest, webserviceRequest, modelMateRequest);
                    dispatch(modelMateResponse);
                }
            }).catch((error) => {
                const consumer = this._responseConsumerFor('TEXT_BODY_ERROR');
                const modelMateResponse = consumer(error, response, fetchRequest, webserviceRequest, modelMateRequest);
                dispatch(modelMateResponse);
            });
        }).catch((error) => {
            const consumer = this._responseConsumerFor('CONNECTION_ERROR');
            const modelMateResponse = consumer(error, fetchRequest, webserviceRequest, modelMateRequest);
            dispatch(modelMateResponse);
        });
    }
}
