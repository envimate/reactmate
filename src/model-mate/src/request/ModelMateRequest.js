import {isAString} from "../util/TypeAssertions";

export class ModelMateRequest {

    static createNew(actionTypeStringOrObject, payload) {
        let properties;
        if (isAString(actionTypeStringOrObject)) {
            const type = actionTypeStringOrObject;
            properties = [{name: 'type', value: type}];
        } else {
            if (!actionTypeStringOrObject.type) {
                throw new Error("ModelMateRequest has to be either a string interpreted as type or an object with a property named 'type'.");
            }
            properties = Object.getOwnPropertyNames(actionTypeStringOrObject).map(propertyName => ({name: propertyName, value: actionTypeStringOrObject[propertyName]}));
        }

        if (payload) {
            properties.push({name: 'payload', value: payload});
        }
        return new ModelMateRequest(properties);

    }

    constructor(properties) {
        const request = {
            isModelMateRequest: true,
        };
        for (let prop of properties) {
            request[prop.name] = prop.value
        }
        return request;
    }
}