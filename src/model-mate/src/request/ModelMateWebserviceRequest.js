import {isAString} from "../util/TypeAssertions";

export default class ModelMateWebserviceRequest {

    static createNew(pathStringOrObject, bodyObject) {
        let properties;
        if (isAString(pathStringOrObject)) {
            const path = pathStringOrObject;
            properties = [{name: 'path', value: path}, {name: 'method', value: 'GET'}];
        } else {
            if (!pathStringOrObject.path) {
                throw new Error("ModelMateWebserviceRequest has to be either a string interpreted as path or an object with an path property.");
            }
            if (pathStringOrObject.method) {
                pathStringOrObject.method = pathStringOrObject.method.toUpperCase();
            } else {
                pathStringOrObject.method = 'GET';
            }
            properties = Object.getOwnPropertyNames(pathStringOrObject).map(propertyName => ({name: propertyName, value: pathStringOrObject[propertyName]}));
        }
        if (bodyObject) {
            properties.push({name: 'bodyObject', value: bodyObject});
        }
        return new ModelMateWebserviceRequest(properties);
    }

    constructor(properties) {
        const modelMateWbServiceRequest = {
            isModelMateWebserviceRequest: true,
        };
        for (let prop of properties) {
            modelMateWbServiceRequest[prop.name] = prop.value
        }
        return modelMateWbServiceRequest;
    }

}