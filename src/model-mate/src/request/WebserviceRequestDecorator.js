const DEFAULT_WEBSERVICE_DECORATORS = [
    aWebserviceDecorator((fetchRequest, webserviceRequest) => {
        let options = {...fetchRequest.options, method: webserviceRequest.method, mode: 'cors'};
        const headers = {};
        if (webserviceRequest.method === 'GET') {
            headers['Accept'] = 'application/json';
        } else {
            headers['Content-Type'] = 'application/json';
            options.body = JSON.stringify(webserviceRequest.bodyObject)
        }
        const addGETRequestDataHeader = webserviceRequest.GETRequestData;
        if (addGETRequestDataHeader) {
            headers['GETRequestData'] = JSON.stringify(webserviceRequest.GETRequestData);
        }
        options = {...options, headers: headers};
        return {...fetchRequest, options: options};
    }),
    aWebserviceDecorator((fetchRequest, webserviceRequest, modelMateRequest, configuration) => {
        const apiBasePath = configuration.apiBasePath ? configuration.apiBasePath : '';
        const path = webserviceRequest.path;
        const url = `${apiBasePath}${path}`;
        return {...fetchRequest, url: url};
    })
];

export function aWebserviceDecorator(decoratorFunction) {
    return decoratorFunction;
}

export function getDefaultWebserviceRequestDecorators() {
    return DEFAULT_WEBSERVICE_DECORATORS;
}