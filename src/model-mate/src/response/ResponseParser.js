
import {ModelMateWebserviceResponse} from "./ModelMateWebserviceResponse";

const DEFAULT_RESPONSE_PARSER = (response, textBody) =>  ModelMateWebserviceResponse.fromResponse(response, textBody);

export function getDefaultResponseParser(){
    return DEFAULT_RESPONSE_PARSER;
}