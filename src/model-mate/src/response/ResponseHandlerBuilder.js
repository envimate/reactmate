export class ResponseHandlerBuilder {
    constructor(requestType) {
        this._requestType = requestType;
        this._handlersForTypeMap = new Map();
    }

    using(handler) {
        this._handler = handler;
        return this;
    }

    onStart() {
        this._addHandlerForType(`${this._requestType}_START`, this._handler);
        return this;
    }

    onFinish() {
        this._addHandlerForType(`${this._requestType}_SUCCESS`, this._handler);
        this._addHandlerForType(`${this._requestType}_ERROR_USE_CASE`, this._handler);
        this._addHandlerForType(`${this._requestType}_ERROR_UNSUPPORTED_RESPONSE`, this._handler);
        this._addHandlerForType(`${this._requestType}_TEXT_BODY_ERROR`, this._handler);
        this._addHandlerForType(`${this._requestType}_ERROR_CONNECTION`, this._handler);
        return this;
    }

    onSuccess() {
        this._addHandlerForType(`${this._requestType}_SUCCESS`, this._handler);
        return this;
    }

    onUseCaseError() {
        this._addHandlerForType(`${this._requestType}_ERROR_USE_CASE`, this._handler);
        return this;
    }

    onUnsupportedResponse() {
        this._addHandlerForType(`${this._requestType}_ERROR_UNSUPPORTED_RESPONSE`, this._handler);
        return this;
    }

    onTextBodyError() {
        this._addHandlerForType(`${this._requestType}_TEXT_BODY_ERROR`, this._handler);
        return this;
    }

    onConnectionError() {
        this._addHandlerForType(`${this._requestType}_ERROR_CONNECTION`, this._handler);
        return this;
    }

    onErrors() {
        this._addHandlerForType(`${this._requestType}_ERROR_USE_CASE`, this._handler);
        this._addHandlerForType(`${this._requestType}_ERROR_UNSUPPORTED_RESPONSE`, this._handler);
        this._addHandlerForType(`${this._requestType}_TEXT_BODY_ERROR`, this._handler);
        this._addHandlerForType(`${this._requestType}_ERROR_CONNECTION`, this._handler);
        return this;
    }

    onAllNonUseCaseErrors() {
        this._addHandlerForType(`${this._requestType}_ERROR_UNSUPPORTED_RESPONSE`, this._handler);
        this._addHandlerForType(`${this._requestType}_TEXT_BODY_ERROR`, this._handler);
        this._addHandlerForType(`${this._requestType}_ERROR_CONNECTION`, this._handler);
        return this;
    }

    onCustomSuffix(suffix) {
        this._addHandlerForType(`${this._requestType}${suffix}`, this._handler);
        return this;
    }

    forAllEvents(){
        this._addHandlerForType(`${this._requestType}_START`, this._handler);
        this._addHandlerForType(`${this._requestType}_SUCCESS`, this._handler);
        this._addHandlerForType(`${this._requestType}_ERROR_USE_CASE`, this._handler);
        this._addHandlerForType(`${this._requestType}_ERROR_UNSUPPORTED_RESPONSE`, this._handler);
        this._addHandlerForType(`${this._requestType}_TEXT_BODY_ERROR`, this._handler);
        this._addHandlerForType(`${this._requestType}_ERROR_CONNECTION`, this._handler);
        return this;
    }

    _addHandlerForType(actionSubType, handler) {
        let handlers;
        if (this._handlersForTypeMap.has(actionSubType)) {
            handlers = this._handlersForTypeMap.get(actionSubType);
        } else {
            handlers = [];
            this._handlersForTypeMap.set(actionSubType, handlers);
        }
        handlers.push(handler);
    }


    build() {
        return this._handlersForTypeMap;
    }

    asReduxReducer(initialState = {}) {
        const reducersByAction = this._handlersForTypeMap;
        return (state = initialState, action) => {
            let newState = state;
            const reducers = reducersByAction.get(action.type);
            if (reducers) {
                for (let reducer of reducers) {
                    newState = reducer(newState, action);
                }
            }
            return newState;
        }
    }
}