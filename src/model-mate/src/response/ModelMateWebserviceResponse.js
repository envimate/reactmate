export class ModelMateWebserviceResponse {
    constructor(status, responseBodyText) {
        this.status = status;
        this.responseBodyText = responseBodyText;
    }

    isSuccess() {
        return this.status === 200;
    }

    isUseCaseError() {
        return this.status === 400;
    }

    parse() {
        const parsed = JSON.parse(this.responseBodyText);
        return parsed;
    }

    toString() {
        return JSON.stringify(this);
    }

    static fromResponse(response, responseBodyText) {
        return new ModelMateWebserviceResponse(response.status, responseBodyText);
    }
}