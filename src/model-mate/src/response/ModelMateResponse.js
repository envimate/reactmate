export class ModelMateResponse {

    static forStart(fetchRequest, webserviceRequest, modelMateRequest) {
        return {
            type: `${modelMateRequest.type}_START`,
            isModelMateResponse: true,
            modelMateRequestActionType: modelMateRequest.type,
            modelMateRequest: modelMateRequest,
            modelMateWebserviceRequest: webserviceRequest,
            fetchRequest: fetchRequest,
        };
    }


    static forSuccess(textBody, parsedResponse, response, fetchRequest, webserviceRequest, modelMateRequest) {
        return {
            type: `${modelMateRequest.type}_SUCCESS`,
            isModelMateResponse: true,
            modelMateRequestActionType: modelMateRequest.type,
            payload: parsedResponse.parse(),
            responseTextBody: textBody,
            parsedResponse: parsedResponse,
            response: response,
            modelMateRequest: modelMateRequest,
            modelMateWebserviceRequest: webserviceRequest,
            fetchRequest: fetchRequest,
        };
    }

    static forUseCaseError(textBody, parsedResponse, response, fetchRequest, webserviceRequest, modelMateRequest) {
        return {
            type: `${modelMateRequest.type}_ERROR_USE_CASE`,
            isModelMateResponse: true,
            modelMateRequestActionType: modelMateRequest.type,
            payload: parsedResponse.parse(),
            responseTextBody: textBody,
            parsedResponse: parsedResponse,
            response: response,
            modelMateRequest: modelMateRequest,
            modelMateWebserviceRequest: webserviceRequest,
            fetchRequest: fetchRequest,
        };
    }

    static forUnsupportedResponse(textBody, parsedResponse, response, fetchRequest, webserviceRequest, modelMateRequest) {
        return {
            type: `${modelMateRequest.type}_ERROR_UNSUPPORTED_RESPONSE`,
            isModelMateResponse: true,
            modelMateRequestActionType: modelMateRequest.type,
            payload: parsedResponse.parse(),
            responseTextBody: textBody,
            parsedResponse: parsedResponse,
            response: response,
            modelMateRequest: modelMateRequest,
            modelMateWebserviceRequest: webserviceRequest,
            fetchRequest: fetchRequest,
        };
    }

    static forTextBodyError(error, response, fetchRequest, webserviceRequest, modelMateRequest) {
        return {
            type: `${modelMateRequest.type}_TEXT_BODY_ERROR`,
            isModelMateResponse: true,
            modelMateRequestActionType: modelMateRequest.type,
            error: error,
            response: response,
            modelMateRequest: modelMateRequest,
            modelMateWebserviceRequest: webserviceRequest,
            fetchRequest: fetchRequest,
        };
    }

    static forConnectionError(error, fetchRequest, webserviceRequest, modelMateRequest) {
        return {
            type: `${modelMateRequest.type}_CONNECTION_ERROR`,
            isModelMateResponse: true,
            modelMateRequestActionType: modelMateRequest.type,
            error: error,
            modelMateRequest: modelMateRequest,
            modelMateWebserviceRequest: webserviceRequest,
            fetchRequest: fetchRequest,
        };
    }

}