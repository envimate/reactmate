import {ModelMateResponse} from "./ModelMateResponse";

const DEFAULT_RESPONSE_CONSUMERS = [
    {
        responseState: 'START',
        handler: (fetchRequest, webserviceRequest, modelMateRequest) => ModelMateResponse.forStart(fetchRequest, webserviceRequest, modelMateRequest)
    },
    {
        responseState: 'SUCCESS',
        handler: (textBody, parsedResponse, response, fetchRequest, webserviceRequest, modelMateRequest) => {
            return ModelMateResponse.forSuccess(textBody, parsedResponse, response, fetchRequest, webserviceRequest, modelMateRequest)
        }
    },
    {
        responseState: 'USE_CASE_ERROR',
        handler: (textBody, parsedResponse, response, fetchRequest, webserviceRequest, modelMateRequest) => {
            return ModelMateResponse.forUseCaseError(textBody, parsedResponse, response, fetchRequest, webserviceRequest, modelMateRequest)
        }
    },
    {
        responseState: 'UNSUPPORTED_RESPONSE',
        handler: (textBody, parsedResponse, response, fetchRequest, webserviceRequest, modelMateRequest) => {
            return ModelMateResponse.forUnsupportedResponse(textBody, parsedResponse, response, fetchRequest, webserviceRequest, modelMateRequest)
        }
    },
    {
        responseState: 'TEXT_BODY_ERROR',
        handler: (error, response, fetchRequest, webserviceRequest, modelMateRequest) => {
            return ModelMateResponse.forTextBodyError(error, response, fetchRequest, webserviceRequest, modelMateRequest)
        }
    },
    {
        responseState: 'CONNECTION_ERROR',
        handler: (error, fetchRequest, webserviceRequest, modelMateRequest) => {
            return ModelMateResponse.forConnectionError(error, fetchRequest, webserviceRequest, modelMateRequest)
        }
    },
];

export function getDefaultResponseConsumers() {
    return new Map(DEFAULT_RESPONSE_CONSUMERS.map(consumer => [consumer.responseState, consumer.handler]));
}