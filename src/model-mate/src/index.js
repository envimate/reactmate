import {aModelMate, ModelMate} from "./ModelMate";
import {ModelMateResponse} from "./response/ModelMateResponse";
import {aDataProvider} from "./data-providing/DataProvider";
import {LocalResponse} from "./data-providing/LocalResponse";

export {
    aModelMate,
    ModelMate,
    ModelMateResponse,
    aDataProvider,
    LocalResponse
}
