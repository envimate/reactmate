export function aDataProvider() {
    return new DataProviderBuilder();
}

export class DataProviderBuilder {

    constructor() {
        this._dataProviders = new Map();
        this._currentRequestConfigurationBuilder = null;
    }

    forRequest(type) {
        if (this._dataProviders.has(type)) {
            throw new Error(`Tried to add DataProvider for type ${type}, but there was already one registered.`);
        }
        if (!!this._currentRequestConfigurationBuilder) {
            const requestConfiguration = this._currentRequestConfigurationBuilder.build();
            this._addDataProviderFor(requestConfiguration);
        }
        this._currentRequestConfigurationBuilder = new RequestConfigurationBuilder();
        this._currentRequestConfigurationBuilder.forRequest(type);
        return this;
    }

    mappingItToWebserviceRequestVia(mappingHandler) {
        this._currentRequestConfigurationBuilder.mappingItToWebserviceRequestVia(mappingHandler);
        return this;
    }

    simulatingWebserviceResponseLocally(localHandler, configuration) {
        this._currentRequestConfigurationBuilder.simulatingWebserviceResponseLocally(localHandler, configuration);
        return this;
    }


    _addDataProviderFor(requestConfiguration) {
        const {mappingHandler, localHandler, type, configuration} = requestConfiguration;
        if (this._dataProviders.has(type)) {
            throw new Error(`Tried to add DataProvider for type ${type}, but there was already one registered.`);
        } else {
            const dataProvider = dataProviderFor(mappingHandler, localHandler, configuration);
            this._dataProviders.set(type, dataProvider);
        }
    }

    build() {
        if (!!this._currentRequestConfigurationBuilder) {
            const requestConfiguration = this._currentRequestConfigurationBuilder.build();
            this._addDataProviderFor(requestConfiguration);
        }
        return this._dataProviders;
    }
}

class RequestConfigurationBuilder {

    constructor() {
        this._type = null;
        this._mappingHandler = null;
        this._localHandler = null;
        this._configuration = {};
    }

    forRequest(type) {
        this._type = type;
        return this;
    }

    mappingItToWebserviceRequestVia(mappingHandler) {
        this._mappingHandler = mappingHandler;
        return this;
    }

    simulatingWebserviceResponseLocally(localHandler, configuration = {}) {
        this._localHandler = localHandler;
        this._configuration = configuration;
        return this;
    }

    build() {
        if (!this._type) {
            throw new Error(`Unfinished DataProvider configuration for request. Missing argument 'type' (being set with function ${this.forRequest.name}`);
        }
        if (!this._mappingHandler) {
            throw new Error(`Unfinished DataProvider configuration for request. Missing argument 'mappingHandler' (being set with function ${this.mappingItToWebserviceRequestVia.name}`);
        }
        if (!this._localHandler) {
            throw new Error(`Unfinished DataProvider configuration for request. Missing argument 'localHandler' (being set with function ${this.simulatingWebserviceResponseLocally.name}`);
        }
        return {
            type: this._type,
            mappingHandler: this._mappingHandler,
            localHandler: this._localHandler,
            configuration: this._configuration,
        }
    }
}

function dataProviderFor(mappingHandler, localHandler, configuration) {
    return new DataProvider(mappingHandler, localHandler, configuration);
}

class DataProvider {

    constructor(mappingHandler, localHandler, configuration) {
        this._mappingHandler = mappingHandler;
        this._localHandler = localHandler;
        this._configuration = configuration;
    }

    mapToWebserviceRequest(modelMateRequest) {
        return this._mappingHandler(modelMateRequest);
    }

    getLocalWebserviceHandler(){
        return this._localHandler;
    }

    getRemoteWebserviceHandler(){
        return (decoratedWebserviceRequest) => fetch(decoratedWebserviceRequest.url, decoratedWebserviceRequest.options);
    }

    getConfiguration(){
        return this._configuration;
    }

}
