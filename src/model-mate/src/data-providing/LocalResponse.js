export class LocalResponse {

    static forSuccess(payloadAsJson={}, statusText = 'Success', statusCode = 200) {
        const textPayload = JSON.stringify(payloadAsJson);
        return promiseForResponse(statusCode, statusText, textPayload);
    }

    static forUseCaseError(payloadAsJson={}, statusText = 'UseCaseError', statusCode = 400) {
        const textPayload = JSON.stringify(payloadAsJson);
        return promiseForResponse(statusCode, statusText, textPayload);
    }

    static forUnsupportedResponse(payloadAsJson={}, statusText = 'UnsupportedResponse', statusCode = 500) {
        const textPayload = JSON.stringify(payloadAsJson);
        return promiseForResponse(statusCode, statusText, textPayload);

    }

    static forTextBodyError(error) {
        return promiseForTextBodyError(error);
    }

    static forConnectionError(error) {
        return promiseForError(error);
    }

}

function responseFor(statusCode, statusText, textPayload) {
    const init = {"status": statusCode, "statusText": statusText};
    const blob = new Blob([textPayload]);
    return new Response(blob, init);
}

function promiseForResponse(statusCode, statusText, textPayload) {
    const response = responseFor(statusCode, statusText, textPayload);
    return new Promise((resolve) => resolve(response));
}

function promiseForTextBodyError(error) {
    const responseStub = {
        text() {
            return promiseForError(error)
        }
    };
    return new Promise((resolve, reject) => resolve(responseStub));
}

function promiseForError(error) {
    return new Promise((resolve, reject) => reject(error));
}