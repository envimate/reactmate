import router from "hoc-little-router";

class HocLittleRouterEnhancer {
    constructor(url) {
        this.url = url;
    }

    enhanceComponent(component) {
        return router(this.url)(component);
    }
}

export const onlyForUrl = (url) => {
    return new HocLittleRouterEnhancer(url);
};
