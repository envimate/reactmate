import babel from 'rollup-plugin-babel'
import uglify from 'rollup-plugin-uglify'

const env = process.env.NODE_ENV;
const config = {
    input: 'src/index.js',
    plugins: [
        babel({
            exclude: 'node_modules/**',
            plugins: ['external-helpers'],
        })],
    external:[
        "hoc-little-router",
    ],
    output:{
        globals: {
            'hoc-little-router': 'hoc-little-router',
        },
    }
};

if (env === 'es') {
    config.output = {...config.output, format: 'es', name: 'react-mate-hoc-little-router-integration', indent: false, file: 'es/react-mate-hoc-little-router-integration.js'};
}

if (env === 'cjs') {
    config.output = {...config.output, format: 'cjs', name: 'react-mate-hoc-little-router-integration', indent: false, file: 'lib/react-mate-hoc-little-router-integration.js'};
}

if (env === 'umd') {
    config.output = {...config.output, format: 'umd', name: 'react-mate-hoc-little-router-integration', indent: false, file: 'dist/react-mate-hoc-little-router-integration.umd.js'}
}

if (env === 'umd-min') {
    config.output = {...config.output, format: 'umd', name: 'react-mate-hoc-little-router-integration', indent: false, file: 'dist/react-mate-hoc-little-router-integration.umd.min.js'};
    config.plugins.push(
        uglify({
            compress: {
                pure_getters: true,
                unsafe: true,
                unsafe_comps: true,
                warnings: false
            }
        })
    )
}

export default config