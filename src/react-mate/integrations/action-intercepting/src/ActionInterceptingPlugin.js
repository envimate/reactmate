import {aPluginNamed} from "react-mate";
import * as _ from "lodash";

class ActionInterceptingPlugin {

    constructor() {
        this.interceptors = [];
    }

    intercepting(filter, stringOrHandler) {
        this.interceptors.push(createActionInterceptor(filter, stringOrHandler));
        return this;
    }

    build() {
        const actionInterceptingMiddleware = store => next => action => {
            const functionsToExecuteAfterwards = [];
            const methods = {
                block: () => true,
                propagate: next,
                dispatchAfterwards: (action) => {
                    functionsToExecuteAfterwards.push(() => store.dispatch(action))
                },
                executeAfterwards: (func) => {
                    functionsToExecuteAfterwards.push(func);
                }
            };

            let atLeastOneInterceptorTriggered = false;
            for (let {filter, handler} of this.interceptors) {
                if (filter(action, store)) {
                    atLeastOneInterceptorTriggered = true;
                    handler(action, store, methods);
                }
            }
            if (functionsToExecuteAfterwards.length > 0) {
                for (let functionToExecute of functionsToExecuteAfterwards) {
                    functionToExecute();
                }
            }
            if (!atLeastOneInterceptorTriggered) {
                next(action);
            }
        };

        return aPluginNamed('ActionInterceptingPlugin')
            .withMiddleware(actionInterceptingMiddleware);
    }
}

function createActionInterceptor(filter, stringOrHandler) {
    if (!filter) {
        throw new Error("Missing filter function for ActionInterceptingPlugin.")
    }
    if (!_.isFunction(filter)) {
        throw new Error("Got an invalid filter function for ActionInterceptingPlugin.")
    }
    if (!stringOrHandler) {
        throw new Error("Missing handler function for ActionInterceptingPlugin.")
    }
    let handlerFunction;
    if (_.isFunction(stringOrHandler)) {
        handlerFunction = stringOrHandler;
    } else if (_.isString(stringOrHandler)) {
        handlerFunction = (action) => (action.type === stringOrHandler)
    } else {
        throw new Error("Got an invalid handler function for ActionInterceptingPlugin. Expected a function or a string")
    }
    return {
        filter: filter,
        handler: handlerFunction
    }
}

export const anActionInterceptorPlugin = () => new ActionInterceptingPlugin();