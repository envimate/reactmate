import {LoadingBar, loadingBarMiddleware, loadingBarReducer} from 'react-redux-loading-bar'
import {aSceneNamed} from "react-mate";

class ReactReduxLoadingBarPluginBuilder {
    constructor() {
        this.actionTypeSuffixPending = null;
        this.actionTypeSuffixFulfilled = null;
        this.actionTypeSuffixRejected = null;
        this.customActionTypeSuffixConfigured = false;
    }

    withPendingActionTypeSuffix(actionTypeSuffix) {
        this.actionTypeSuffixPending = actionTypeSuffix;
        this.customActionTypeSuffixConfigured = true;
        return this;
    }

    withFulfilledActionTypeSuffix(actionTypeSuffix) {
        this.actionTypeSuffixFulfilled = actionTypeSuffix;
        this.customActionTypeSuffixConfigured = true;
        return this;
    }

    withRejectedActionTypeSuffix(actionTypeSuffix) {
        this.actionTypeSuffixFulfilled = actionTypeSuffix;
        this.customActionTypeSuffixConfigured = true;
        return this;
    }

    build() {
        const customPromiseTypeSuffixes = this.customActionTypeSuffixConfigured ? [
            this.actionTypeSuffixPending ? this.actionTypeSuffixPending : 'PENDING',
            this.actionTypeSuffixFulfilled ? this.actionTypeSuffixFulfilled : 'FULFILLED',
            this.actionTypeSuffixRejected ? this.actionTypeSuffixRejected : 'REJECTED',
        ] : null;

        return aSceneNamed('loadingBar')
            .rendering(
                LoadingBar
            )
            .withCustomReducer('loadingBar', loadingBarReducer)
            .withMiddleware(loadingBarMiddleware({
                promiseTypeSuffixes: customPromiseTypeSuffixes,
            }));
    }
}

export const aLoadingBar = () => new ReactReduxLoadingBarPluginBuilder();
