import {aPluginNamed} from "react-mate";

let dispatcher;
let syncActivityFinished = false;
let actionQueue = [];

export const Dispatch = (action) => {
    dispatcher(action);
};


export const AsyncDispatch = (action) => {
    AsyncExecute(() => Dispatch(action))
};

export const AsyncExecute = (action) => {
    actionQueue = actionQueue.concat([action]);

    if (syncActivityFinished) {
        flushQueue();
    }
};

function flushQueue() {
    const currentQueue = actionQueue;
    actionQueue = [];
    currentQueue.forEach(action => action());
}

const asyncDispatchMiddleware = store => next => action => {
    syncActivityFinished = false;

    next(action);

    syncActivityFinished = true;
    flushQueue();
};

export const DispatchPlugin = aPluginNamed('DispatchPlugin')
    .withMiddleware(asyncDispatchMiddleware)
    .withInitializer((store) => {dispatcher = (action) => {
        return store.dispatch(action);
    }}, "dispatch plugin initializer");