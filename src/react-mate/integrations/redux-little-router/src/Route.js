import {isAString} from "./TypeAssertions";
import {isAnObject} from "./TypeAssertions";

export default class Route {

    static fromStringOrObject(stringOrObject) {
        if (isAString(stringOrObject)) {
            const urlString = stringOrObject;
            return {url: urlString, title: urlString};
        } else if (isAnObject(stringOrObject)) {
            const obj = stringOrObject;
            if (obj.url && !obj.title) {
                return {...obj, title:obj.url};
            } else {
                if(!obj.url) {
                    Route.throwCreationError();
                }else{
                    Route.throwErrorForNotAllowedPropertyTitle();
                }
            }
        } else {
            Route.throwCreationError();
        }
    }

    static throwCreationError() {
        throw new Error("Expected an url string or an object with property 'url'.");
    }
    static throwErrorForNotAllowedPropertyTitle() {
        throw new Error("Property 'title' is not allowed on route object, because it is internally used to match your routes. Use 'name' or similar instead.");
    }

}
