import {initializeCurrentLocation, LOCATION_CHANGED, push, PUSH, routerForBrowser} from 'redux-little-router';
import {aReduxPluginNamed, createReactMateActionListener, ReactMateLogger} from "react-mate";
import Route from "./Route";

/* Location Changed Payload
{
  pathname: '/messages/a-user-has-no-name',
  route: '/messages/:user',
  params: {
    user: 'a-user-has-no-name'
  },
  query: { // if your `history` instance uses `useQueries`
    some: 'thing'
  },
  search: '?some=thing',
  result: {
    arbitrary: 'data that you defined in your routes object!'
    parent: { // for nested routes only
      // contains the result of the parent route,
      // which contains each other parent route's
      // result recursively
    }
  }
}
 */
const createEnhancedRoutingMiddleware = (redirects, pageNotFound, authenticationHandler, unauthorizedRedirectPage) => {
    return (store) => (next) => (action) => {
        if (action.type === PUSH || action.type === LOCATION_CHANGED) {
            const redirectTarget = redirects[action.payload.pathname];
            if (redirectTarget) {
                ReactMateLogger.log('redirect', redirectTarget);
                return store.dispatch(push(redirectTarget));
            }
        }
        if (pageNotFound && action.type === LOCATION_CHANGED) {
            if (!action.payload.result) {
                ReactMateLogger.log('404', action.payload);
                return store.dispatch(push(pageNotFound));
            }
        }

        if (unauthorizedRedirectPage && action.type === LOCATION_CHANGED) {
            if (action.payload.result) {
                if (!authenticationHandler(action.payload.pathname, action.payload.result, action.payload)) {
                    return store.dispatch(push(unauthorizedRedirectPage));
                }
            }
        }
        return next(action);
    };
};

class ReduxLittleRouterPluginBuilder {
    constructor() {
        this.routes = {};
        this.redirects = {};
        this.pageNotFound = null;
        this.authenticationHandler = () => true;
        this.unauthorizedRedirectPage = null;
    }

    withRoute(route) {
        const routeObject = Route.fromStringOrObject(route);
        this.routes[routeObject.url] = routeObject;
        return this;
    }

    withRedirect(route, target) {
        const routeObject = Route.fromStringOrObject(route);
        const targetObject = Route.fromStringOrObject(target);
        this.redirects[routeObject.url] = targetObject.url;
        return this;
    }

    withPageNotFound(route) {
        const routeObject = Route.fromStringOrObject(route);
        this.pageNotFound = routeObject.url;
        return this;
    }

    isAuthorized(authenticationHandler) {
        this.authenticationHandler = authenticationHandler;
        return this;
    }

    withUnauthorizedRedirectPage(unauthorizedRedirectPage) {
        this.unauthorizedRedirectPage = unauthorizedRedirectPage;
        return this;
    }

    build() {
        const ReduxLittleRouter = routerForBrowser({
            routes: this.routes
        });

        const initReduxLittleRouter = (store) => {
            const initialLocation = store.getState().router;
            if (initialLocation) {
                store.dispatch(initializeCurrentLocation(initialLocation));
            }
        };

        return aReduxPluginNamed('ReduxLittleRouter')
            .withEnhancer(ReduxLittleRouter.enhancer)
            .withReducer('router', ReduxLittleRouter.reducer)
            .withMiddleware(createEnhancedRoutingMiddleware(this.redirects, this.pageNotFound, this.authenticationHandler, this.unauthorizedRedirectPage))
            .withMiddleware(ReduxLittleRouter.middleware)
            .withInitializer(initReduxLittleRouter);
    }
}

export const aRouter = () => new ReduxLittleRouterPluginBuilder();

export const uponLocationChangedActions = function () {
    return new LocationChangedListenerBuilder();
};

class LocationChangedListenerBuilder {
    toUrl(url) {
        if (this.leavingFromUrl) {
            throw new Error('Used both, toUrl and leavingFromUrl - you must choose between one of them though.');
        }
        this.url = url;
        return this;
    }

    leavingFrom(url) {
        if (this.url) {
            throw new Error('Used both, toUrl and leavingFromUrl - you must choose between one of them though.');
        }
        this.leavingFromUrl = url;
        return this;
    }

    using(listener) {
        this.listener = listener;
        return this;
    }

    build() {
        const listener = this.listener;
        let reactMateActionListener;
        if (this.url) {
            reactMateActionListener = createReactMateActionListener(LOCATION_CHANGED, (action, store) => {
                const state = store.getState();
                const router = state.router;
                if (router.route && router.route === this.url) {
                    listener(action, store);
                }
            });
        } else if (this.leavingFromUrl) {
            reactMateActionListener = createReactMateActionListener(LOCATION_CHANGED, (action, store) => {
                const state = store.getState();
                const router = state.router;
                if (router.previous && router.previous.route === this.leavingFromUrl) {
                    listener(action, store);
                }
            });
        } else {
            throw new Error('Used none of toUrl or leavingFromUrl - you must choose between one of them though.');
        }
        return [reactMateActionListener];
    }
}

export const locationChangedActions = () => new LocationChangedReducerBuilder();

class LocationChangedReducerBuilder {
    toUrl(url) {
        this.url = url;
        return this;
    }

    using(reducer) {
        this.reducer = reducer;
        return this;
    }

    build() {
        const reducer = this.reducer;
        return new Map([[LOCATION_CHANGED, [(state, action) => {
            if (action.type === LOCATION_CHANGED) {
                return reducer(state, action);
            }
        }]]]);
    }
}
