import {aPluginNamed} from "react-mate";
import {aModelMate} from "model-mate";

class ModelMatePlugin {
    constructor() {
        this._modelMateBuilder = aModelMate();
    }

    with(dataProviders) {
        this._modelMateBuilder.with(dataProviders);
        return this;
    }

    isLocal(isLocal) {
        this._modelMateBuilder.isLocal(isLocal);
        return this;
    }

    withApiBasePath(apiBasePath) {
        this._modelMateBuilder.withApiBasePath(apiBasePath);
        return this;
    }

    withConfiguration(configuration) {
        this._modelMateBuilder.withConfiguration(configuration);
        return this;
    }

    withAWebserviceRequestDecorator(webserviceRequestDecorator, position) {
        this._modelMateBuilder.withAWebserviceRequestDecorator(webserviceRequestDecorator, position);
        return this;
    }

    setAllWebserviceRequestDecorator(webserviceRequestDecorators) {
        this._modelMateBuilder.setAllWebserviceRequestDecorator(webserviceRequestDecorators);
        return this;
    }

    setResponseParser(responseParser) {
        this._modelMateBuilder.setResponseParser(responseParser);
        return this;
    }

    withAResponseConsumer(responseState, responseConsumer) {
        this._modelMateBuilder.withAResponseConsumer(responseState, responseConsumer);
        return this;
    }

    setAllResponseConsumers(responseConsumerMap) {
        this._modelMateBuilder.setAllResponseConsumers(responseConsumerMap);
        return this;
    }

    withADataProviderMiddleware(dataProviderMiddleware){
        this._modelMateBuilder.withADataProviderMiddleware(dataProviderMiddleware);
        return this;
    }

    build() {
        const modelMateMiddleWare = this._modelMateBuilder.build();
        const reactMatePluginBuilder = aPluginNamed('ModelMate')
            .withMiddleware(modelMateMiddleWare);
        return reactMatePluginBuilder.build();
    }
}

export const aModelMatePlugin = () => new ModelMatePlugin();