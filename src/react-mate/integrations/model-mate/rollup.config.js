import babel from 'rollup-plugin-babel'
import uglify from 'rollup-plugin-uglify'

const env = process.env.NODE_ENV;
const config = {
    input: 'src/index.js',
    plugins: [
        babel({
            exclude: 'node_modules/**',
            plugins: ['external-helpers'],
        })],
    external: [
        "react-mate",
        "model-mate",
    ],
    output: {
        globals: {
            'react-mate': 'react-mate',
            'model-mate': 'model-mate'
        },
    }
};

if (env === 'es') {
    config.output = {...config.output, format: 'es', name: 'react-mate-model-mate-integration', indent: false, file: 'es/react-mate-model-mate-integration.js'};
}

if (env === 'cjs') {
    config.output = {...config.output, format: 'cjs', name: 'react-mate-model-mate-integration', indent: false, file: 'lib/react-mate-model-mate-integration.js'};
}

if (env === 'umd') {
    config.output = {...config.output, format: 'umd', name: 'react-mate-model-mate-integration', indent: false, file: 'dist/react-mate-model-mate-integration.umd.js'}
}

if (env === 'umd-min') {
    config.output = {...config.output, format: 'umd', name: 'react-mate-model-mate-integration', indent: false, file: 'dist/react-mate-model-mate-integration.umd.min.js'};
    config.plugins.push(
        uglify({
            compress: {
                pure_getters: true,
                unsafe: true,
                unsafe_comps: true,
                warnings: false
            }
        })
    )
}

export default config