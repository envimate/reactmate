import {createLogger} from "redux-logger";
import {aReduxPluginNamed} from "react-mate";


class ReduxLoggerPluginBuilder {
    constructor() {
        this.options = undefined;
    }

    withOptions(options) {
        this.options = options;
        return this;
    }

    build() {
        return aReduxPluginNamed('ReduxLogger')
            .withMiddleware(createLogger(this.options));
    }
}

export const aReduxLogger = () => new ReduxLoggerPluginBuilder();
