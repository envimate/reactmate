import {connect} from "react-redux";

class ReactReduxEnhancer {
    constructor(mapStateToProps, mapDispatchToProps) {
        this.mapStateToProps = mapStateToProps;
        this.mapDispatchToProps = mapDispatchToProps;
    }

    enhanceComponent(component) {
        return connect(this.mapStateToProps, this.mapDispatchToProps)(component);
    }
}

export const connectedToReduxUsing = (mapStateToProps, mapDispatchToProps) => {
    return new ReactReduxEnhancer(mapStateToProps, mapDispatchToProps);
};
