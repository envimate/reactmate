class ReducerBuilder {

    constructor() {
        this.reducersByAction = new Map();
        this.initialState = {};
    }

    withInitialState(initialState) {
        this.initialState = initialState;
        return this;
    }

    reducingActionType(actionType, handler) {
        let reducers = this.reducersByAction.get(actionType);
        if (!reducers) {
            reducers = [];
            this.reducersByAction.set(actionType, reducers);
        }
        reducers.push(handler);
        return this;
    }

    reducing(reducersBuilder) {
        const reducersForActionTypeMap = reducersBuilder.build();
        reducersForActionTypeMap.forEach((reducers, actionType) => {
            reducers.forEach((reducer) => {
                this.reducingActionType(actionType, reducer);
            }, this);
        }, this);
        return this;
    }

    build() {
        const initialState = this.initialState;
        const reducersByAction = this.reducersByAction;
        return (state = initialState, action) => {
            let newState = state;
            const reducers = reducersByAction.get(action.type);
            if (reducers) {
                for (let reducer of reducers) {
                    newState = reducer(newState, action);
                }
            }
            return newState;
        }
    }
}

export function aReducer() {
    return new ReducerBuilder();
}
