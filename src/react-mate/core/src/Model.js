class ReduxReducer {
    constructor(namespace, reducerFunction, description) {
        this.namespace = namespace;
        this.reducerFunction = reducerFunction;
        this.description = description;
    }
}

export const createReduxReducer = (namespace, reducerFunction, description) => {
    const reducer = new ReduxReducer(namespace, reducerFunction, description);
    if (!reducer.namespace) {
        throw new Error(`Invalid ReduxReducer - namespace is required: ${JSON.stringify(reducer)}`);
    }
    if (!reducer.reducerFunction) {
        throw new Error(`Invalid ReduxReducer - reducerFunction is required: ${JSON.stringify(reducer)}`);
    }
    if (!reducer.description) {
        reducer.description = `ReduxReducer for namespace ${reducer.namespace}`;
    }
    return reducer;
};

class ReduxStoreEnhancer {
    constructor(enhancer, description) {
        this.enhancer = enhancer;
        this.description = description;
    }
}

export const createReduxStoreEnhancer = (enhancer, description) => {
    const reduxStoreEnhancer = new ReduxStoreEnhancer(enhancer, description);
    if (!reduxStoreEnhancer.enhancer) {
        throw new Error(`Invalid ReduxStoreEnhancer - enhancer is required: ${JSON.stringify(reduxStoreEnhancer)}`);
    }
    if (!reduxStoreEnhancer.description) {
        reduxStoreEnhancer.description = `ReduxStoreEnhancer ${reduxStoreEnhancer.enhancer}`;
    }
    return reduxStoreEnhancer;
};

class ReduxMiddleware {
    constructor(middleware, description) {
        this.middleware = middleware;
        this.description = description;
    }
}

export const createReduxMiddleware = (middleware, description) => {
    const reduxMiddleware = new ReduxMiddleware(middleware, description);
    if (!reduxMiddleware.middleware) {
        throw new Error(`Invalid ReduxMiddleware - middleware is required: ${JSON.stringify(reduxMiddleware)}`);
    }
    if (!reduxMiddleware.description) {
        reduxMiddleware.description = `ReduxMiddleware ${reduxMiddleware.middleware}`;
    }
    return reduxMiddleware;
};

class ReactMateActionListener {
    constructor(actionType, actionListenerFunction, description) {
        this.actionType = actionType;
        this.actionListenerFunction = actionListenerFunction;
        this.description = description;
    }
}

export const createReactMateActionListener = (actionType, actionListenerFunction, description) => {
    const actionListener = new ReactMateActionListener(actionType, actionListenerFunction, description);
    if (!actionListener.actionType) {
        throw new Error(`Invalid ReactMateActionListener definition - actionType is required: ${JSON.stringify(actionListener)}`);
    }
    if (!actionListener.actionListenerFunction) {
        throw new Error(`Invalid ReactMateActionListener definition - actionListenerFunction is required: ${JSON.stringify(actionListener)}`);
    }
    if (!actionListener.description) {
        actionListener.description = `ReactMateActionListener for actionType ${actionListener.actionType}`;
    }
    return actionListener;
};

class ReactMateStoreStateCreator {

    constructor(stateCreationFunction, description) {
        this.stateCreationFunction = stateCreationFunction;
        this.description = description;
    }
}

export const createReactMateInitialStoreStateCreator = (stateCreationFunction, description) => {
    const storeStateCreator = new ReactMateStoreStateCreator(stateCreationFunction, description);
    if (!storeStateCreator.stateCreationFunction) {
        throw new Error(`Invalid ReactMateStoreStateCreator definition - stateCreationFunction is required: ${JSON.stringify(storeStateCreator)}`);
    }
    if (!storeStateCreator.description) {
        storeStateCreator.description = `ReactMateStoreStateCreator`;
    }
    return storeStateCreator;
};

class ReactMateInitializer {
    constructor(initializerFunction, description) {
        this.initializerFunction = initializerFunction;
        this.description = description;
    }
}

export const createReactMateInitializer = (initializerFunction, description) => {
    const reactMateInitializer = new ReactMateInitializer(initializerFunction, description);
    if (!reactMateInitializer.initializerFunction) {
        throw new Error(`Invalid ReactMateInitializer definition - initializerFunction is required: ${JSON.stringify(reactMateInitializer)}`);
    }
    if (!reactMateInitializer.description) {
        reactMateInitializer.description = `ReactMateInitializer`;
    }
    return reactMateInitializer;
};
