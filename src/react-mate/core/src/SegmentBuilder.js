import {createReactMateActionListener, createReduxMiddleware, createReduxReducer} from "./Model";
import {multiMapFromArray} from "./util/MultiMapBuilder";
import Logger from "./util/Logger";

class SegmentBuilder {
    constructor(name) {
        this.name = name;
        this.reactComponent = null;
        this.reducers = [];
        this.reducersByAction = new Map();
        this.actionListeners = [];
        this.initialState = {};
        this.middlewares = [];
    }

    rendering(component, ...componentEnhancers) {
        this.reactComponent = component;
        for (const componentEnhancer of componentEnhancers) {
            this.reactComponent = componentEnhancer.enhanceComponent(this.reactComponent);
        }
        return this;
    }

    acting(eventListenerBuilder) {
        const listeners = eventListenerBuilder.build();
        listeners.forEach((listener) => {
            this.actingUpon(listener.actionType, listener.actionListenerFunction, listener.description);
        });
        return this;
    }

    actingUpon(actionType, listener, description) {
        const actionListenerDescription = description ? description : `ActionListener of scene ${this.name} for actionType ${actionType}`;
        this.actionListeners.push(createReactMateActionListener(actionType, listener, actionListenerDescription));
        return this;
    }

    withTheInitialState(initialState) {
        this.initialState = initialState;
        return this;
    }

    reducing(reducersBuilder) {
        const reducersByAction = reducersBuilder.build();
        const saved_this = this;
        reducersByAction.forEach((reducers, actionType) => {
            reducers.forEach((reducer) => {
                saved_this.reducingAction(actionType, reducer);
            });
        });
        return this;
    }

    reducingAction(actionType, handler) {
        let reducers = this.reducersByAction.get(actionType);
        if (!reducers) {
            reducers = [];
            this.reducersByAction.set(actionType, reducers);
        }
        reducers.push(handler);
        return this;
    }

    withMiddleware(middleware, description) {
        const middlewareDescription = description ? description : `ReduxMiddleware of scene ${this.name}`;
        this.middlewares.push(createReduxMiddleware(middleware, middlewareDescription));
        return this;
    }

    withCustomReducer(namespace, reducer, description) {
        const reducerDescription = description ? description : `Reducer of scene ${this.name} for namespace ${namespace}`;
        this.reducers.push(createReduxReducer(namespace, reducer, reducerDescription));
        return this;
    }

    build() {
        const sceneReducerDescription = `Reducer for scene namespace ${this.name}`;
        const sceneReducer = createReduxReducer(this.name, actionReducerFor(this.initialState, this.reducersByAction), sceneReducerDescription);
        this.reducers.push(sceneReducer);
        this.middlewares.push(createReduxMiddleware(actionListenerMiddleware(this.actionListeners), `action listener middleware for scene ${this.name}`));
        return {
            name: this.name,
            isSegment: true,
            reactComponent: this.reactComponent,
            reducers: this.reducers,
            enhancers: [],
            middlewares: this.middlewares,
            initializers: []
        }
    }
}

export const aSegmentNamed = (name) => {
    return new SegmentBuilder(name);
};

const actionReducerFor = (initialState, reducersByAction) => {
    return (state = initialState, action) => {
        let newState = state;
        const reducers = reducersByAction.get(action.type);
        if (reducers) {
            for (let reducer of reducers) {
                newState = reducer(newState, action);
            }
        }
        return newState;
    };
};

const actionListenerMiddleware = (actionListeners) => {
    const actionListenersByActionType = multiMapFromArray(actionListeners,
        (actionListener) => actionListener.actionType,
        (actionListener) => actionListener
    );
    return (store) => (next) => (action) => {
        const returnValue = next(action);
        const listeners = actionListenersByActionType.get(action.type);
        if (listeners) {
            for (let listener of listeners) {
                Logger.log(`notifying ${listener.description}`, action);
                listener.actionListenerFunction(action, store);
            }
        }
        return returnValue;
    };
};
