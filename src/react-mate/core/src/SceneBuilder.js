import {createReactMateActionListener, createReduxMiddleware, createReduxReducer} from "./Model";
import {multiMapFromArray} from "./util/MultiMapBuilder";
import Logger from "./util/Logger";
import {combineReducers} from "redux";

class SceneBuilder {
    constructor(name) {
        this.name = name;
        this.reactComponent = null;
        this.reducers = [];
        this.reducersByAction = new Map();
        this.namespacedReducer = null;
        this.actionListeners = [];
        this.initialState = {};
        this.middlewares = [];
    }

    rendering(component, ...componentEnhancers) {
        this.reactComponent = component;
        for (const componentEnhancer of componentEnhancers) {
            this.reactComponent = componentEnhancer.enhanceComponent(this.reactComponent);
        }
        return this;
    }

    acting(eventListenerBuilder) {
        const listeners = eventListenerBuilder.build();
        listeners.forEach((listener) => {
            this.actingUpon(listener.actionType, listener.actionListenerFunction, listener.description);
        });
        return this;
    }

    actingUpon(actionType, listener, description) {
        const actionListenerDescription = description ? description : `ActionListener of scene ${this.name} for actionType ${actionType}`;
        this.actionListeners.push(createReactMateActionListener(actionType, listener, actionListenerDescription));
        return this;
    }

    withTheInitialState(initialState) {
        this.initialState = initialState;
        return this;
    }

    reducing(reducersBuilder) {
        this._ensureNoConflictingNamespacedReducer();
        const reducersByAction = reducersBuilder.build();
        const saved_this = this;
        reducersByAction.forEach((reducers, actionType) => {
            reducers.forEach((reducer) => {
                saved_this.reducingAction(actionType, reducer);
            });
        });
        return this;
    }

    reducingAction(actionType, handler) {
        this._ensureNoConflictingNamespacedReducer();
        let reducers = this.reducersByAction.get(actionType);
        if (!reducers) {
            reducers = [];
            this.reducersByAction.set(actionType, reducers);
        }
        reducers.push(handler);
        return this;
    }

    withCustomReducer(namespace, reducer, description) {
        this._ensureReducerSpaceCanBeNamespaced(namespace);
        const reducerDescription = description ? description : `Reducer of scene ${this.name} for namespace ${namespace}`;
        const reduxReducer = createReduxReducer(namespace, reducer, reducerDescription);
        this._addNamespacedReducer(reduxReducer);
        return this;
    }

    withCustomReducerInRootSpace(namespace, reducer, description) {
        if(namespace === this.name){
            throw new Error(`Cannot add reducer for same namespace as the scene is called, because the scene might register reducer under the same name.`);
        }
        const reducerDescription = description ? description : `Reducer of scene ${this.name} for namespace ${namespace}`;
        this.reducers.push(createReduxReducer(namespace, reducer, reducerDescription));
        return this;
    }

    withMiddleware(middleware, description) {
        const middlewareDescription = description ? description : `ReduxMiddleware of scene ${this.name}`;
        this.middlewares.push(createReduxMiddleware(middleware, middlewareDescription));
        return this;
    }

    _ensureNoConflictingNamespacedReducer() {
        if (this._isNamespaced()) {
            throw new Error("Can not add reducer as the reducer space is already namespaced by using function 'withCustomReducer'.");
        }
    }

    _ensureReducerSpaceCanBeNamespaced(namespace) {
        if (this.reducers.length > 0 || this.reducersByAction.length > 0) {
            throw new Error("Can not add a namespaced reducer as the reducer space is already handled by using the functions 'reducing' or 'reducingAction'.");
        }
        if (namespace.toLowerCase() === 'scene') {
            throw new Error(`Can not add reducer for namespace '${namespace}', as this is a reserved snamespace.`);
        }
    }

    _isNamespaced() {
        return this.namespacedReducer;
    }

    _addNamespacedReducer(reducer) {
        if (this.namespacedReducer) {
            if (this.namespacedReducer[reducer.namespace]) {
                throw new Error(`You have multiple reducers configured to be responsible for the same namespace ${reducer.namespace}`);
            } else {
                this.namespacedReducer[reducer.namespace] = reducer.reducerFunction;
            }
        } else {
            this.namespacedReducer = {
                [reducer.namespace]: reducer.reducerFunction,
            }
        }
    }

    build() {
        const sceneReducerDescription = `Reducer for scene namespace ${this.name}`;
        if (this._isNamespaced()) {
            this.reducers.push(createReduxReducer(this.name, combineReducers(this.namespacedReducer)));
            Logger.log("Adding namespaced reducers for "+this.name+": "+JSON.stringify(this.namespacedReducer));
        } else {
            const sceneReducer = createReduxReducer(this.name, actionReducerFor(this.initialState, this.reducersByAction), sceneReducerDescription);
            Logger.log("Adding scene reducer reducers for "+this.name+": "+JSON.stringify(this.namespacedReducer));
            this.reducers.push(sceneReducer);
        }
        this.middlewares.push(createReduxMiddleware(actionListenerMiddleware(this.actionListeners), `action listener middleware for scene ${this.name}`));
        return {
            name: this.name,
            isScene: true,
            reactComponent: this.reactComponent,
            reducers: this.reducers,
            enhancers: [],
            middlewares: this.middlewares,
            initializers: []
        }
    }
}

export const aSceneNamed = (name) => {
    return new SceneBuilder(name);
};

const actionReducerFor = (initialState, reducersByAction) => {
    return (state = initialState, action) => {
        let newState = state;
        const reducers = reducersByAction.get(action.type);
        if (reducers) {
            for (let reducer of reducers) {
                newState = reducer(newState, action);
            }
        }
        return newState;
    };
};

const actionListenerMiddleware = (actionListeners) => {
    const actionListenersByActionType = multiMapFromArray(actionListeners,
        (actionListener) => actionListener.actionType,
        (actionListener) => actionListener
    );
    return (store) => (next) => (action) => {
        const returnValue = next(action);
        const listeners = actionListenersByActionType.get(action.type);
        if (listeners) {
            for (let listener of listeners) {
                Logger.log(`notifying ${listener.description}`, action);
                listener.actionListenerFunction(action, store);
            }
        }
        return returnValue;
    };
};
