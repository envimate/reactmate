import React, {Component} from "react";
import Logger from "./util/Logger";

class Scenes extends Component {
    render() {
        Logger.log('rendering scenes', this.props.reactMate.scenes);
        const cssClasses = this.props.className ? this.props.className : undefined;
        const style = this.props.style ? this.props.style : undefined;
        return (
            <div id='scenes' className={cssClasses} style={style}>
                {this.props.reactMate.scenes.map((scene, index) => React.createElement(scene, {key: `Scene_${index}`}))}
            </div>
        );
    }
}

export default Scenes;
