import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import Logger from "./util/Logger";

class ReactMateBuilder {
    constructor() {
        this.pluginBuilders = [];
    }

    withPlugin(pluginBuilder) {
        this.pluginBuilders.push(pluginBuilder);
        return this;
    }

    withScene(sceneBuilder) {
        this.pluginBuilders.push(sceneBuilder);
        return this;
    }

    withSegment(segmentBuilder) {
        this.pluginBuilders.push(segmentBuilder);
        return this;
    }

    build() {
        const combinedReducerDefinitions = {};
        const combinedEnhancers = [];
        const combinedMiddlewares = [];
        const scenes = [];
        const segments = [];
        let initialStoreState = {};
        const plugins = this.pluginBuilders.map(pluginBuilder => pluginBuilder.build());
        plugins.forEach(plugin => {
            Logger.log('processing plugin', plugin);
            plugin.reducers.forEach(reducer => {
                Logger.log(`processing ${reducer.description}`, reducer);
                if (combinedReducerDefinitions[reducer.namespace]) {
                    throw new Error(`You have multiple reducers configured to be responsible for the same namespace ${reducer.namespace}`);
                }
                combinedReducerDefinitions[reducer.namespace] = reducer.reducerFunction;
            });
            if (plugin.enhancers) {
                plugin.enhancers.forEach(enhancer => {
                    Logger.log(`processing ${enhancer.description}`, enhancer);
                    combinedEnhancers.push(enhancer.enhancer);
                });
            }
            if (plugin.middlewares) {
                plugin.middlewares.forEach(middleware => {
                    Logger.log(`processing ${middleware.description}`, middleware);
                    combinedMiddlewares.push(middleware.middleware);
                });
            }
            if (plugin.reactComponent) {
                if (plugin.isScene) {
                    Logger.log(`processing React Scene`, plugin.reactComponent);
                    scenes.push(plugin.reactComponent);
                } else {
                    Logger.log(`processing React Segment`, plugin.reactComponent);
                    segments.push({
                        name: plugin.name,
                        reactComponent: plugin.reactComponent,
                    })
                }
            }
            if (plugin.initialStoreStateCreators) {
                plugin.initialStoreStateCreators.forEach(initialStoreStateCreator => {
                    Logger.log(`processing ${initialStoreStateCreator.description}`, initialStoreStateCreator);
                    initialStoreState = initialStoreStateCreator.stateCreationFunction(initialStoreState);
                });
            }
        });
        Logger.log(`combining reducers`, combinedReducerDefinitions);
        const reducers = combineReducers(combinedReducerDefinitions);
        Logger.log(`applying middlewares ${combinedMiddlewares}`, combinedMiddlewares);
        const appliedMiddleware = applyMiddleware(...combinedMiddlewares);

        const composeEnhancers = process.env.NODE_ENV !== 'production' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

        const storeEnhancers = composeEnhancers(
            ...combinedEnhancers,
            appliedMiddleware
        );

        Logger.log(`creating store`);
        let store;
        if(initialStoreState && initialStoreState !== {}) {
            store = createStore(reducers, initialStoreState, storeEnhancers);
        }else{
            store = createStore(reducers, storeEnhancers);
        }

        plugins.forEach(plugin => {
            if (plugin.initializers) {
                plugin.initializers.forEach(initializer => {
                    Logger.log(`initializing ${initializer.description}`);
                    initializer.initializerFunction(store)
                });
            }
        });

        return {
            store: store,
            scenes: scenes,
            segments: segments
        };
    }
}

export const aReactMate = () => new ReactMateBuilder();
