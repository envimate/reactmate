export const multiMapFromArray = (array, keyExtractor, valueExtractor) => {
    const map = new Map();
    array.forEach((element) => {
        const key = keyExtractor(element);
        let multiValuesArray = map.get(key);
        if (!multiValuesArray) {
            multiValuesArray = [];
            map.set(key, multiValuesArray);
        }
        const value = valueExtractor(element);
        multiValuesArray.push(value);
    });
    return map;
};
