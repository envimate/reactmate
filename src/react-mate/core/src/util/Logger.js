class Logger {
    constructor() {
        this.enabled = false;
    }

    enable() {
        this.enabled = true;
    }

    disable() {
        this.enabled = false;
    }

    log(message, detailArg) {
        if (this.enabled) {
            if (detailArg) {
                console.log(message, detailArg);
            } else {
                console.log(message);
            }
        }
    }
}

let logger = new Logger();

export default logger;
