import {createReactMateInitializer, createReactMateInitialStoreStateCreator, createReduxMiddleware, createReduxReducer, createReduxStoreEnhancer} from "./Model";

class PluginBuilder {
    constructor(name) {
        this.name = name;
        this.reducers = [];
        this.enhancers = [];
        this.middlewares = [];
        this.initialStoreStateCreators = [];
        this.initializers = [];
    }

    withReducer(namespace, reducer, description) {
        const reducerDescription = description ? description : `ReduxReducer of plugin ${this.name} for namespace ${namespace}`;
        this.reducers.push(createReduxReducer(namespace, reducer, reducerDescription));
        return this;
    }

    withEnhancer(enhancer, description) {
        const enhancerDescription = description ? description : `ReduxStoreEnhancer of plugin ${this.name}`;
        this.enhancers.push(createReduxStoreEnhancer(enhancer, enhancerDescription));
        return this;
    }

    withMiddleware(middleware, description) {
        const middlewareDescription = description ? description : `ReduxMiddleware of plugin ${this.name}`;
        this.middlewares.push(createReduxMiddleware(middleware, middlewareDescription));
        return this;
    }

    withInitialStoreState(stateCreationFunction, description) {
        const initialStoreStateDescription = description ? description : `ReactMateInitialStoreState of plugin ${this.name}`;
        this.initialStoreStateCreators.push(createReactMateInitialStoreStateCreator(stateCreationFunction, initialStoreStateDescription));
        return this;
    }

    withInitializer(initializer, description) {
        const intializerDescription = description ? description : `ReactMateInitializer of plugin ${this.name}`;
        this.initializers.push(createReactMateInitializer(initializer, intializerDescription));
        return this;
    }

    build() {
        return {
            reducers: this.reducers.map((reducerDefinition) => createReduxReducer(reducerDefinition.namespace, reducerDefinition.constructor)),
            enhancers: this.enhancers,
            middlewares: this.middlewares,
            initialStoreStateCreators: this.initialStoreStateCreators,
            initializers: this.initializers
        }
    }
}

export const aReduxPluginNamed = (name) => new PluginBuilder(name);

export const aPluginNamed = (name) => new PluginBuilder(name);
