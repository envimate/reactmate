import React, {Component} from "react";
import Logger from "./util/Logger";

class Segment extends Component {
    render() {
        const segments = this.props.reactMate.segments;
        const name = this.props.name;
        Logger.log(`searching for segment ${name} in:`, segments);
        const segmentsWithName = segments.filter(segment => segment.name === name);
        if (segmentsWithName.length === 0) {
            throw new Error(`Could not find a segment named ${name} in list of segments ${segments}`);
        } else if (segmentsWithName.length > 1) {
            throw new Error(`Found multiple segments named ${name}: ${segmentsWithName}`);
        } else {
            const segment = segmentsWithName[0];
            Logger.log(`rendering segment ${name}`, segment);
            return React.createElement(segment.reactComponent);
        }
    }
}

export default Segment;
