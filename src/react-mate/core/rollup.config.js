import babel from 'rollup-plugin-babel'
import uglify from 'rollup-plugin-uglify'

const env = process.env.NODE_ENV;
const config = {
    input: 'src/index.js',
    plugins: [
        babel({
            exclude: 'node_modules/**',
            plugins: ['external-helpers'],
        })],
    external: [
        "react",
        "redux",
    ],
    output: {
        globals: {
            'react': 'react',
            'redux': 'redux',
        },
    }
};

if (env === 'es') {
    config.output = {...config.output, format: 'es', name: 'react-mate', indent: false, file: 'es/react-mate.js'};
}

if (env === 'cjs') {
    config.output = {...config.output, format: 'cjs', name: 'react-mate', indent: false, file: 'lib/react-mate.js'};
}

if (env === 'umd') {
    config.output = {...config.output, format: 'umd', name: 'react-mate', indent: false, file: 'dist/react-mate.umd.js'}
}

if (env === 'umd-min') {
    config.output = {...config.output, format: 'umd', name: 'react-mate', indent: false, file: 'dist/react-mate.umd.min.js'};
    config.plugins.push(
        uglify({
            compress: {
                pure_getters: true,
                unsafe: true,
                unsafe_comps: true,
                warnings: false
            }
        })
    )
}

export default config